﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class GroupMXH:UserEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int GroupID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// 
        public string GroupName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SuperAdmin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PrivacyMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string GrouPicture { get; set; }= "/Theme/Asset/Ava/avatar-trang-4.jpg";
    }
    public class GroupEx : GroupMXH
    {
        /// <summary>
        /// 
        /// </summary>
        public List<PostEx> Posts { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<UserEx> Users { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public UserInGroup UserInGroup { get; set; }

        
    }
}
