﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class NotificationResult
    {
        public int Notification_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Actor_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Entity_Type_Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string userFirstName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string userLastName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Avatar { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int EntityId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string GroupName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CommentOnPost { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PostInGroup { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TimeSpan { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int LikePost { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int GroupJoin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Friend CheckFriend { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public UserInGroup CheckUserInGroup { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ContentPostLiked { get; set; }
    }
}
