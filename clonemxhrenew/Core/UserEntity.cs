﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class UserEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserFirstName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserLastName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string WallPaper { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Avartar { get; set; } = "/Theme/Asset/Ava/avatar-trang-4.jpg";
        /// <summary>
        /// 
        /// </summary>
        public int Sex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateDay { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime BirthDay { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RePass { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Skill { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Location { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Education { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Note { get; set; }
    }
    public class UserEx : UserEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int Friendcount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int FollowCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int FollowerCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int BlockedCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Friend FriendCheck { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Follow Follow { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Block Block { get; set; }

    }
    public static class ImageType
    {
        /// <summary>
        /// 
        /// </summary>
        public static int Wall = 1;
        /// <summary>
        /// 
        /// </summary>
        public static int Avatar = 2;
    }

    
}
