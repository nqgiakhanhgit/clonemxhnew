﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Friend:UserEntity
    {
        public int FriendID { get; set; }
        public int UserID1 { get; set; }
        public int UserID2 { get; set; }
        public int Status { get; set; }
    }
}
