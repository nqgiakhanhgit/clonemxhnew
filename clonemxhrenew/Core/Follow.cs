﻿using clonemxh.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Follow:UserEntity
    {
        public int FollowId { get; set; }
        public int UserId1 { get; set; }
        public int UserId2 { get; set;}
    }
    public class Block:UserEntity
    {
        public int BlockID { get; set; }
        public int UserId1 { get; set; }
        public int UserId2 { get; set; }
    }

}
