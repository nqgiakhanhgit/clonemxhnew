﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class UserInGroup:UserEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int UserInGroupId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int GroupID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RoleID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SupperAdmin { get;set; }
    }
}
