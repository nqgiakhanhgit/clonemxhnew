﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class EntityType
    {
        /// <summary>
        /// 
        /// </summary>
        public static int Post = 1;
        /// <summary>
        /// 
        /// </summary>
        public static int Comment = 2;
        /// <summary>
        /// 
        /// </summary>
        public static int AddFriend = 3;
        /// <summary>
        /// 
        /// </summary>
        public static int AceptFriend = 4;
        /// <summary>
        /// 
        /// </summary>
        public static int Follow = 5;
        /// <summary>
        /// 
        /// </summary>
        public static int JoinGroupRequest = 6;
        /// <summary>
        /// 
        /// </summary>
        public static int JoinGroupAcept = 7;
        /// <summary>
        /// 
        /// </summary>
        public static int LikePost = 8; 
    }
}
