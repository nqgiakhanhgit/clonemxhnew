﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace clonemxh.Models
{
    public class _BasePaginationQueryInput
    {
        /// <summary>
        /// Trang cần hiển thị
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// Số lượng
        /// </summary>
        public int PageSize { get; set; }
    }
}