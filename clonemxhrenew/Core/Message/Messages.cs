﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Message
{
    public class Messages:UserEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ConverSationID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SenderId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TimeSpan { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ParentID { get; set; }   
    }
    public class MessageInput:Messages
    {
        /// <summary>
        /// 
        /// </summary>
        public string userid { get; set; }
    }
}
