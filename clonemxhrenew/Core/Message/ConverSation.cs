﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Message
{
    public class ConverSation
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CreatorId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ConverSationAvatar { get; set; } = "";
        /// <summary>
        ///  Tên cuộc trò chuyện
        ///  Nếu không có tên thì lấy list User ra.
        /// </summary>
        public string ConverSationName { get; set; }
    }

    public class ConverSationResult:ConverSation
    {
        /// <summary>
        /// 
        /// </summary>
        public List<UserEntity>Users { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LastMess { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TimeSpan { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Seen { get; set; }
    }
}
