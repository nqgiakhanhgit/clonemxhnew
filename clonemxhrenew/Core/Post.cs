﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Core
{
    public class Post:UserEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int PostID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; } = "";
        /// <summary>
        /// 
        /// </summary>
        public int GroupID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PrivacyMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TimeSpan {  get; set; }   
    }
    public class PostEx:Post
    {
        /// <summary>
        /// 
        /// </summary>
        public List<Image> Images { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<Comment> Comments { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Like UserLiked { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CommentCount { get; set; } = 0;
        /// <summary>
        /// 
        /// </summary>
        public int UsersLikePost { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SuperAdmin { get;set; }
        /// <summary>
        /// 
        /// </summary>
        public string GroupName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string GroupPicture { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RoleID { get; set; }

    }
}
