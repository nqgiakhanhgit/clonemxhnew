﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clonemxh.Models
{
    public class _BasePaginationQueryResult
    {
        /// <summary>
        /// Trang cần lấy dữ liệu
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// Số dòng dữ liệu hiển thị trên mỗi trang
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Tổng số dòng dữ liệu truy vấn được
        /// </summary>
        public long RowCount { get; set; }
        /// <summary>
        /// Tổng số trang truy vấn được
        /// </summary>
        public long PageCount
        {
            get
            {
                //Luôn là 1 trang, kể cả khi không có dữ liệu
                if (RowCount <= 0)
                    return 1;

                if (PageSize <= 0)
                    return 1;

                long count = RowCount / PageSize;
                if (RowCount % PageSize > 0)
                    count += 1;
                return count;
            }
        }

    }
}