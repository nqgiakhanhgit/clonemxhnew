﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Core
{
    public class Search
    {

        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int GroupID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string GroupName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string GroupPicture { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Avatar { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserFirstName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserLastName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PostID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SuperAdmin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RoleID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PostPrivacyMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RowNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TimeSpan { get; set; }  
        /// <summary>
        /// 
        /// </summary>
        public int CommentCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int LikeCount { get; set; }
    }
}
