﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Comment:UserEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int CommentID { get; set; }
        /// <summary>
        /// ID cha (sử dụng để làm repcomment)
        /// </summary>
        public int ParentID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RepCommentCount {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PostID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateAt {  get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TimeSpan { get; set; }

    }

}
