﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class Like:UserEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public int LikeID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PostID { get; set; }
    }
    public class LikeEx:Like
    {
        /// <summary>
        /// 
        /// </summary>
        public List<UserEntity> Users { get; set; } 
    }
}
