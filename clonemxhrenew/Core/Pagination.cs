﻿using clonemxh.Models;
using Core.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Core
{
    /// <summary>
    /// 
    /// </summary>
    public class UserSearchingCondition : _BasePaginationQueryInput
    {
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SearchText { get; set; }

    }
    /// <summary>
    /// 
    /// </summary>
    public class UserPaginationResult : _BasePaginationQueryResult
    {
        /// <summary>
        /// 
        /// </summary>
        public List<UserEntity> ListFollow { get; set; }

    }
    /// <summary>
    /// 
    /// </summary>
    public class PostSearchingCondition : _BasePaginationQueryInput
    {
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int GroupID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PrivacyMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SearchText { get; set; } = "";
    }
    /// <summary>
    /// 
    /// </summary>
    public class PostPaginationResult : _BasePaginationQueryResult
    {
        /// <summary>
        /// 
        /// </summary>
        public List<PostEx> ListPost { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class CommentSearchingCondition : _BasePaginationQueryInput
    {
        /// <summary>
        /// 
        /// </summary>
        public int PostId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ParentID { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class CommentPaginationResult : _BasePaginationQueryResult
    {
        /// <summary>
        /// 
        /// </summary>
        public List<Comment> Data { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RoleID { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class GroupSearchingCondition : _BasePaginationQueryInput
    {
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class GroupPaginationResult : _BasePaginationQueryResult
    {
        /// <summary>
        /// 
        /// </summary>
        public List<GroupMXH> Data { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UserInGroupSearchingCondition : _BasePaginationQueryInput
    {
        /// <summary>
        /// 
        /// </summary>
        public int GroupID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UserInGroupPaginationResult : _BasePaginationQueryResult
    {
        /// <summary>
        /// 
        /// </summary>
        public List<UserInGroup> Data { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class SearchCondition : _BasePaginationQueryInput
    {
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int GroupID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SearchText { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class SearchResult : _BasePaginationQueryResult
    {
        /// <summary>
        /// 
        /// </summary>
        public List<Search> Data { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<PostEx> Posts { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class NotificationSearchingCondition : _BasePaginationQueryInput
    {
        /// <summary>
        /// 
        /// </summary>
        public int Notifier_id { get; set; }
    } 
    public class NotificationPanginationResult : _BasePaginationQueryResult
    {
        /// <summary>
        /// 
        /// </summary>
        public List<NotificationResult> Data { get; set; }
    }
    public class MessageSearchingCondition : _BasePaginationQueryInput
    {
        /// <summary>
        /// 
        /// </summary>
        public int userid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// 
        public int ConverSationId { get; set; }
    }
    public class MessagerPaginationResult : _BasePaginationQueryResult
    {
        /// <summary>
        /// 
        /// </summary>
        public List<Messages> Data { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ConverSationID { get; set; } 
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }    
    }
}
