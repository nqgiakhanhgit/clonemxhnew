﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public static class PostType
    {
        /// <summary>
        /// Bài viết bên trong trang cá nhân
        /// </summary>
        public static int ProfilePost = 1;
        /// <summary>
        /// Bài viết bên trong Group
        /// </summary>
        public static int InSideGroupPost = 2;
        /// <summary>
        /// Bài viết ở trang tổng hợp tất cả các nhóm mà user đã tham gia
        /// </summary>
        public static int GroupIndexPost = 3;
        /// <summary>
        /// Bài viết ở trang chủ
        /// </summary>
        public static int HomePost = 4;
    }
}
