﻿using Core;
using Core.Message;
using Data;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bussiness
{
    public class MessageServices
    {
        /// <summary>
        /// 
        /// </summary>
        private static IMessageDAL messdb;
        /// <summary>
        /// 
        /// </summary>
        private static IConverSationDAL converSationdb;
        /// <summary>
        /// 
        /// </summary>
        private static IDeleteConverSationDAL deleteConverSationdb;
        /// <summary>
        /// 
        /// </summary>
        private static IDeleteMesssage deleteMesssagedb;
     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbType"></param>
        /// <param name="connectionString"></param>
        /// <exception cref="Exception"></exception>
        public static void Init(DatabaseType dbType, string connectionString)
        {
            switch (dbType)
            {
                case DatabaseType.SqlServer:
                    messdb = new Data.SQL.MessageDAL(connectionString);
                    converSationdb = new Data.SQL.ConverSationDAL(connectionString);
                    deleteConverSationdb = new Data.SQL.DeleteConverSationDAL(connectionString);
                    deleteMesssagedb = new Data.SQL.DeleteMessageDAL(connectionString);
                    break;
                case DatabaseType.FakeDB:

                    break;
                default:
                    throw new Exception("DataBase Type is not Support");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static int Send(MessageInput model)
        {
            return messdb.Send(model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static List<ConverSationResult> ListConverSation(int userid)
        {
            return messdb.GetConversations(userid).ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        public static List<Messages> ListMessages(MessageSearchingCondition model,out long rowCount,out string Name)
        {
            return messdb.ListMessage(model, out rowCount,out Name).ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static int CountMessage(int userid)
        {
            return messdb.Count(userid);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static bool UpdateSeenMessage(int userid,int conversationid)
        {
            return messdb.UpdateSeen(userid,conversationid);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="useid"></param>
        /// <param name="conversation"></param>
        /// <returns></returns>
        public static int Add(int creator,string useid, out int conversation)
        {
            return converSationdb.Add(creator,useid, out conversation);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="conversationid"></param>
        /// <returns></returns>
        public static bool CheckUserInConverSation(int userid,int conversationid)
        {
            return messdb.CheckUserInConverSation(userid, conversationid) > 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conversationid"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int DeleteConverSation(int conversationid,int userId) =>deleteConverSationdb.Add(conversationid,userId);        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conversationid"></param>
        /// <param name="messageid"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int DeleteMessageOnly(int messageid,int userId) => deleteMesssagedb.Add(messageid, userId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageid"></param>
        /// <returns></returns>
        public static bool DeleteMessage(int messageid)=>deleteMesssagedb.DeleteMessage(messageid);
        
    }
}
