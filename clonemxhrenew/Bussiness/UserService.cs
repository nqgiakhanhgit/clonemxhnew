﻿using Core;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Bussiness
{
    public class UserService
    {
        /// <summary>
        /// 
        /// </summary>
        private static IUserDAL userdb;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbType"></param>
        /// <param name="connectionString"></param>
        /// <exception cref="Exception"></exception>
        public static void Init(DatabaseType dbType, string connectionString)
        {
            switch (dbType)
            {
                case DatabaseType.SqlServer:
                    userdb = new Data.SQL.UserDAL(connectionString);                 
                    break;
                case DatabaseType.FakeDB:

                    break;
                default:
                    throw new Exception("DataBase Type is not Support");
            }
        }
        /// <summary>
        /// Đăng ký
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static int Register(UserEntity user)
        {
            return userdb.Register(user);
        }
        /// <summary>
        /// Đăng nhập
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static UserEntity Login(string email,string password) 
        {
            return userdb.Login(email, password);
        }
        /// <summary>
        /// Thông tin cá nhân
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static UserEx GetProfile (int id)
        {
            return userdb.Get(id);
        }
        /// <summary>
        /// Cập nhật thông tin
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static bool UpdateProfile(UserEntity user)
        {
            return userdb.Update(user);
        }
        /// <summary>
        /// Đăng nhập bằng Email google  facebook
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static UserEntity GetUserByEmail(string email)
        {
            return userdb.GetUserByEmail(email);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="imagetype"></param>
        /// <returns></returns>
        public static string GetImage(int id,int imagetype)
        {
            return userdb.GetImage(id,imagetype);   
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="image"></param>
        /// <param name="imagetype"></param>
        /// <returns></returns>
        public static bool UpdateImage(int id,string image,int imagetype)
        {
            return userdb.UpdateImage(id, image, imagetype);
        }
    }
}
