﻿using Core;
using Data;
using Data.SQL;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bussiness
{
    public class GroupService
    {
        /// <summary>
        /// 
        /// </summary>
        private static IGroupDAL groupdb;
        /// <summary>
        /// 
        /// </summary>
        private static IUserInGroupDAL useringroupdb;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbType"></param>
        /// <param name="connectionString"></param>
        /// <exception cref="Exception"></exception>
        public static void Init(DatabaseType dbType, string connectionString)
        {
            switch (dbType)
            {
                case DatabaseType.SqlServer:
                    groupdb = new Data.SQL.GroupDAL(connectionString);
                    useringroupdb = new Data.SQL.UserInGroupDAL(connectionString);
                    break;
                case DatabaseType.FakeDB:

                    break;
                default:
                    throw new Exception("DataBase Type is not Support");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public static int AddGroup(GroupMXH group)
        {
            return groupdb.Add(group);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static List<GroupMXH> GroupJoinedList(GroupSearchingCondition model, out long rowCount)
        {
            return groupdb.GroupJoinList(model, out rowCount).ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gr"></param>
        /// <returns></returns>
        public static bool UpdateGroup(GroupMXH gr)
        {
            return groupdb.Update(gr);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool DeleteGroup(int id)
        {
            return groupdb.Delete(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GroupMXH Get(int id)
        {
            return groupdb.Get(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInGroup"></param>
        /// <returns></returns>
        public static int AddUserInGroup(UserInGroup userInGroup)
        {
            return useringroupdb.Add(userInGroup);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="groupid"></param>
        /// <returns></returns>
        public static UserInGroup GetUserInGroup(int userid, int groupid)
        {
            return useringroupdb.Get(userid, groupid);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="groupid"></param>
        /// <returns></returns>
        public static bool DeleteUserInGroup(int userid, int groupid)
        {
            return useringroupdb.Delete(userid, groupid);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        public static List<UserInGroup> UserInGroupList(UserInGroupSearchingCondition model, out long rowCount)
        {
            return useringroupdb.UserInGroupList(model, out rowCount).ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInGroup"></param>
        /// <returns></returns>
        public static bool UpdateUserInGroup(UserInGroup userInGroup)
        {
            return useringroupdb.Update(userInGroup);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="groupid"></param>
        /// <returns></returns>
        public static bool UpdateSuperAdmin(int userid, int groupid)
        {
            return useringroupdb.UpdateSuperAdmin(userid, groupid);
        }

    }
}
