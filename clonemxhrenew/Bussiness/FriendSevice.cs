﻿using Core;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace Bussiness
{
    public class FriendSevice
    {
        /// <summary>
        /// 
        /// </summary>
        private static IFriendDAL frienddb;
        /// <summary>
        /// 
        /// </summary>
        private static IBlockDAL blockdb;
        /// <summary>
        /// 
        /// </summary>
        private static IFollowDAL followdb;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbType"></param>
        /// <param name="connectionString"></param>
        /// <exception cref="Exception"></exception>
        public static void Init(DatabaseType dbType, string connectionString)
        {
            switch (dbType)
            {
                case DatabaseType.SqlServer:
                    frienddb = new Data.SQL.FriendDAL(connectionString);
                    followdb = new Data.SQL.FollowDAL(connectionString);
                    blockdb = new Data.SQL.BlockDAL(connectionString);
                    break;
                case DatabaseType.FakeDB:

                    break;
                default:
                    throw new Exception("DataBase Type is not Support");
            }
        }
        /// <summary>
        /// Thêm bạn bè
        /// </summary>
        /// <param name="friend"></param>
        /// <returns></returns>
        public static int AddFriend(Friend friend)
        {
            return frienddb.AddFriend(friend);
        }
        /// <summary>
        /// Hủy Kết bạn
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        public static bool UnFriend (int userid1,int userid2)
        {
            return frienddb.UnFriend(userid1,userid2);  
        }
        public static List<UserEntity> Friendlish(UserSearchingCondition model,out long rowcount)
        {
            return frienddb.FriendList(model, out rowcount).ToList();
        }
        /// <summary>
        /// Chấp nhận kết bạn
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        public static bool Acept(int userid1,int userid2)
        {
            return frienddb.Acept(userid1, userid2);
        }
        /// <summary>
        /// Kiểm tra có phải là bạn bè hay không
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        public static Friend CheckFriend(int userid1,int userid2)
        {
            return frienddb.GetFriend(userid1,userid2);
        }
        /// <summary>
        /// Theo dõi
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        public static int AddFollow(Follow f)
        {
            return followdb.AddFollow(f);
        }
        /// <summary>
        /// Bỏ theo dõi
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        public static bool UnFollow(int userid1,int userid2)
        {
            return followdb.UnFollow(userid1,userid2);
        }
        /// <summary>
        /// Chặn
        /// Dừng theo dõi
        /// Hủy kết bạn
        /// </summary>
        /// <param name="block"></param>
        /// <returns></returns>
        public static int AddBlock(Block block)
        {
            return blockdb.AddBlock(block);
        }
        /// <summary>
        /// Bỏ chặn
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        public static bool UnBlock(int userid1,int userid2) 
        {
            return blockdb.UnBlock(userid1,userid2);
        }
        /// <summary>
        /// Kiểm tra Follow chưa
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        public static Follow CheckFollow(int userid1,int userid2)
        {
            return followdb.CheckFollow(userid1, userid2);
        }
        /// <summary>
        /// Kiểm tra block chưa
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        public static Block CheckBlock (int userid1,int userid2)
        {
            return blockdb.CheckBlock(userid1, userid2);
        }
        /// <summary>
        /// Danh sách user đã theo dõi
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public static List<UserEntity> FollowList(UserSearchingCondition model,out long rowcount)
        {
            return followdb.FollowList(model, out rowcount).ToList();
        }
        /// <summary>
        /// Danh sách đã theo dõi user
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public static List<UserEntity> FollowerList(UserSearchingCondition model,out long rowcount)
        {
            return followdb.FollowerList(model, out rowcount).ToList();
        }
        /// <summary>
        /// Danh sách người dùng đã bị block
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public static List<UserEntity> BlockList(UserSearchingCondition model,out long rowcount)
        {
            return blockdb.BlockList(model, out rowcount).ToList();
        }
        /// <summary>
        /// Đếm bạn bè
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static int FriendCount(int id)
        {
            return frienddb.Count(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static int FolowCount (int id)
        {
            return followdb.FollowCount(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static int FollowerCount (int id)
        {
            return followdb.FollowerCount(id);
        }
        public static int BlockedCount(int id)
        {
            return blockdb.Count(id);
        }
    }
}
