﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.Diagnostics.Eventing.Reader;

namespace Bussiness
{
    public class PostSevice
    {
        /// <summary>
        /// 
        /// </summary>
        private static IPostDAL postdb;
        /// <summary>
        /// 
        /// </summary>
        private static IImageDAL imagedb;
        /// <summary>
        /// 
        /// </summary>
        private static ICommentDAL commentdb;
        /// <summary>
        /// 
        /// </summary>
        private static ILikeDAL likedb;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbType"></param>
        /// <param name="connectionString"></param>
        /// <exception cref="Exception"></exception>
        public static void Init(DatabaseType dbType, string connectionString)
        {
            switch (dbType)
            {
                case DatabaseType.SqlServer:
                    postdb = new Data.SQL.PostDAL(connectionString);
                    imagedb = new Data.SQL.ImageDAL(connectionString);
                    commentdb = new Data.SQL.CommentDAL(connectionString);
                    likedb = new Data.SQL.LikeDAL(connectionString);    
                    break;
                case DatabaseType.FakeDB:

                    break;
                default:
                    throw new Exception("DataBase Type is not Support");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public static int AddPost(Post post) 
        {
            return postdb.Add(post);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<PostEx> ListPost(PostSearchingCondition model,out long rowcount, int postType)
        {
            return postdb.ListPost(model,out rowcount,postType).ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static int AddImage(Image image)
        {
            return imagedb.Add(image);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="postid"></param>
        /// <returns></returns>
        public static List<Image> Images(int postid)
        {
            return imagedb.Images(postid);
        }
        /// <summary>
        /// Xóa ảnh
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool DeleteImage(int id) {
            return imagedb.Delete(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static PostEx GetPost(int id)
        {
            return postdb.Get(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public static bool UpdatePost(Post post)
        {
            return postdb.Update(post);
        }
        public static bool DeletePost(int id)
        {
            return postdb.Delete(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmt"></param>
        /// <returns></returns>
        public static int AddComment(Comment cmt)
        {
            return commentdb.Add(cmt);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parentid"></param>
        /// <returns></returns>
        public static Comment GetComment(int id)
        {
            return commentdb.Get(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowcout"></param>
        /// <returns></returns>
        public static List<Comment> ListComments(CommentSearchingCondition model, out long rowcout,out int RoleID)
        {
            return commentdb.ListComment(model,out rowcout,out RoleID).ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool DeleteComment(int id)
        {
            return commentdb.Delete(id);    
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static int CommentCount(int id)
        {
            return commentdb.Count(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public static bool UpdateComment(Comment comment)
        {
            return commentdb.Update(comment);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static int LikePost (Like data)
        {
            return likedb.Add(data);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Unlike(int id,int userid)
        {
            return likedb.UnLike(id,userid);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static Like GetLike(int id,int userid)
        {
            return likedb.GetLike(id,userid);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static int LikeCount (int id)
        {
            return likedb.Count(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<UserEntity> UsersLikedPost(int id)
        {
            return likedb.UsersLikePost(id).ToList();
        }


    }
}
