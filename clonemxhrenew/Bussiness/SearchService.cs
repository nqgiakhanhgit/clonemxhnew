﻿using Core;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bussiness
{
    public class SearchService
    {
        /// <summary>
        /// 
        /// </summary>
        private static ISearchDAL searchdb;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbType"></param>
        /// <param name="connectionString"></param>
        /// <exception cref="Exception"></exception>
        public static void Init(DatabaseType dbType, string connectionString)
        {
            switch (dbType)
            {
                case DatabaseType.SqlServer:
                    searchdb = new Data.SQL.SearchDAL(connectionString);
                    break;
                case DatabaseType.FakeDB:

                    break;
                default:
                    throw new Exception("DataBase Type is not Support");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        public static List<Search> SearchResult(SearchCondition model , out long rowCount)
        {
            return searchdb.Result(model, out rowCount).ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        public static List<PostEx> ListPost(PostSearchingCondition mode,out long rowCount)
        {
            return searchdb.ListPost(mode, out rowCount).ToList();  
        }
    }
}
