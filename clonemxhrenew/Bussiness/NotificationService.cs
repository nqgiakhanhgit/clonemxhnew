﻿using Core;
using Data;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bussiness
{
    public class NotificationService
    {
        /// <summary>
        /// 
        /// </summary>
        private static INotificationDAL notidb;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbType"></param>
        /// <param name="connectionString"></param>
        /// <exception cref="Exception"></exception>
        public static void Init(DatabaseType dbType, string connectionString)
        {
            switch (dbType)
            {
                case DatabaseType.SqlServer:
                    notidb = new Data.SQL.NotificationDAL(connectionString);
                    break;
                case DatabaseType.FakeDB:

                    break;
                default:
                    throw new Exception("DataBase Type is not Support");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static int AddNotification(NotificationInsert model)
        {
            return notidb.Add(model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public static List<NotificationResult> ListNotification(NotificationSearchingCondition model, out long rowcount)
        {
            return notidb.ListNotificationResult(model, out rowcount).ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static int Count(int id)
        {
            return notidb.Count(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static bool Update(int id, int status)
        {
            return notidb.Update(id, status);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static NotificationResult GetNotification(int id)
        {
            return notidb.Get(id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notifierid"></param>
        /// <returns></returns>
        public static bool Update_All_Notification(int notifierid)
        {
            return notidb.Update_All_Noti(notifierid);
        }
    }
}
