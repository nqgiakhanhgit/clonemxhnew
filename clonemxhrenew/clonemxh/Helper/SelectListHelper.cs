﻿using Bussiness;
using Core;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using System.Web;

namespace clonemxh.Helper
{
    public static class SelectListHelper
    {
        public static List<UserEntity> FriendList(int userid)
        {
          UserSearchingCondition model = new UserSearchingCondition()
          {
              Page = 1,
              PageSize = 10,
              Status=1,
              UserId=userid,
          };

            long rowCount = 0;
            var list = new List<UserEntity>();
            list = FriendSevice.Friendlish(model, out rowCount).ToList();
            return list;
        }
    }
}