﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace clonemxh.Helper
{
    public class UserHelper
    {
        public static bool IsValidPhoneNumber(string phoneNumber)
        {
            try
            {
                // Kiểm tra xem chuỗi số điện thoại có đúng định dạng không bằng biểu thức chính quy (regular expression).
                var phoneRegex = new Regex(@"^(\+\d{1,2}\s?)?(\(\d{3}\)|\d{3})[-.\s]?\d{3}[-.\s]?\d{4}$");
                return phoneRegex.IsMatch(phoneNumber);
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string HashPassword(string password)
        {
            using (var sha256 = System.Security.Cryptography.SHA256.Create())
            {
                byte[] hashedBytes = sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsValidEmail(string email)
        {
            try
            {
                // Kiểm tra xem chuỗi email có đúng định dạng không bằng biểu thức chính quy (regular expression).
                var emailRegex = new Regex(@"^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$");
                return emailRegex.IsMatch(email);
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool ContainsSpecialCharacter(string input)
        {
            // Mảng các ký tự đặc biệt
            char[] specialCharacters = { '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '=', '[', ']', '{', '}', '|', '\\', '/', '<', '>', ',', '.', '?', '~', '`' };

            // Kiểm tra xem mỗi ký tự trong mật khẩu có phải là ký tự đặc biệt không
            foreach (char c in input)
            {
                if (specialCharacters.Contains(c))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static bool IsSqlDateTime(DateTime dateTime)
        {
            return (dateTime >= SqlDateTime.MinValue.Value && dateTime <= SqlDateTime.MaxValue.Value);
        }
    }
}