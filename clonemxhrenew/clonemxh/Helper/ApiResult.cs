﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clonemxh.Helper
{
    public class ApiResult
    {
            /// <summary>
            /// 
            /// </summary>
            public const int SUCCESS = 1;
            /// <summary>
            /// 
            /// </summary>
            public const int FAIL = 0;

            /// <summary>
            /// Mã kết quả
            /// </summary>
            public int Code { get; set; }
            /// <summary>
            /// Thông báo
            /// </summary>
            public string Msg { get; set; }
            /// <summary>
            /// Dữ liệu (nếu có)
            /// </summary>
            public object Data { get; set; }

            /// <summary>
            /// Tạo kết quả thành công (Code = 1) với dữ liệu là data
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static ApiResult CreateSuccessResult(object data = null)
            {
                return new ApiResult()
                {
                    Code = SUCCESS,
                    Msg = "Success",
                    Data = data
                };
            }
            /// <summary>
            /// Tạo kết quả không thành công (Code = 0) với thông báo lỗi msg và data (nếu có)
            /// </summary>
            /// <param name="msg"></param>
            /// <param name="data"></param>
            /// <returns></returns>
            public static ApiResult CreateInsuccessResult(string msg, object data = null)
            {
                return new ApiResult()
                {
                    Code = FAIL,
                    Msg = msg,
                    Data = data
                };
            }
            /// <summary>
            /// Tạo kết quả không thành công trong trường hợp dữ liệu đầu vào không hợp lệ
            /// </summary>
            /// <returns></returns>
            public static ApiResult CreateInvalidInputDataResult()
            {
                return new ApiResult()
                {
                    Code = FAIL,
                    Msg = "Input data is not valid",
                    Data = null
                };
            }
            public static ApiResult CreateInvalidInputDataResult(string msg)
            {
                return new ApiResult()
                {
                    Code = FAIL,
                    Msg = msg,
                    Data = null
                };
            }
            /// <summary>
            /// Tạo kết quả thông thành công trong trường hợp dữ liệu/đối tượng không tồn tại
            /// </summary>
            /// <returns></returns>
            public static ApiResult CreateNotExistsResult()
            {
                return new ApiResult()
                {
                    Code = FAIL,
                    Msg = "Object not found",
                    Data = null
                };
            }
            /// <summary>
            /// Tạo kết quả không thành công khi gặp lỗi Exception
            /// </summary>
            /// <param name="ex"></param>
            /// <returns></returns>
            public static ApiResult CreateExceptionResult(Exception ex, string env = "test")
            {
                //TODO: Ghi lại log lỗi trước khi trả về kết quả
                //...
                if (env == "production")
                    return new ApiResult()
                    {
                        Code = FAIL,
                        Msg = "Lỗi không xác định.", // + ":" + ex.StackTrace,
                        Data = null
                    };
                return new ApiResult()
                {
                    Code = FAIL,
                    Msg = ex.Message, // + ":" + ex.StackTrace,
                    Data = null
                };
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="code"></param>
            /// <param name="msg"></param>
            /// <param name="data"></param>
            /// <returns></returns>
            public static ApiResult CreateResult(int code, string msg, object data = null)
            {
                return new ApiResult()
                {
                    Code = code,
                    Msg = msg,
                    Data = data
                };
            }
    }
}