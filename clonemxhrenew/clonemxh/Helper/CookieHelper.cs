﻿using Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clonemxh.Helper
{
    public class CookieHelper
    {
        //Dịch đi 
        public static string UserToCookieString(UserEntity value)
        {
            try
            {
                return JsonConvert.SerializeObject(value);
            }
           catch(Exception ex) {
                throw ex;
            }
        }
        //Dịch lại
        public static UserEntity CookieToStringUsers(string value)
        {
            try
            {
                return JsonConvert.DeserializeObject<UserEntity>(value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        }
    }