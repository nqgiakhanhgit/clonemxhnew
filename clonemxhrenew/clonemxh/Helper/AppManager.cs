﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace clonemxh.Helper
{
    public static class AppManager
    {
        /// <summary>
        /// Lấy 1 giá trị từ AppSettings.config
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetAppSetting(string name)
        {
            try
            {
                return ConfigurationManager.AppSettings[name];
            }
            catch
            {
                return "";
            }
        }

        private static string _ApplicationName = "";
        /// <summary>
        /// Tên ứng dụng
        /// </summary>
        public static string ApplicationName
        {
            get
            {
                if (_ApplicationName == "")
                {
                    _ApplicationName = GetAppSetting("ApplicationName");
                }
                return _ApplicationName;
            }
        }

        private static string _RootURL = "";
        /// <summary>
        /// Lấy đường dẫn URL của ứng dụng (bao gồm giao thức, tên miền và tên ứng dụng. vd: http://domain.com/application/).
        /// URL luôn kết thúc bởi dấu /
        /// </summary>
        /// <returns></returns>
        public static string RootURL
        {
            get
            {
                if (_RootURL == "")
                {
                    HttpRequest request = HttpContext.Current.Request;
                    string appUrl = HttpRuntime.AppDomainAppVirtualPath;

                    if (appUrl != "/")
                        appUrl += "/";

                    _RootURL = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);
                    //_BaseUrl = string.Format("//{0}{1}", request.Url.Authority, appUrl);
                    if (!_RootURL.EndsWith(@"/"))
                        _RootURL += "/";
                }
                return _RootURL;
            }
        }

        private static string _Language = "";
        /// <summary>
        /// Ngôn ngữ mặc định sử dụng trên ứng dụng
        /// </summary>
        public static string Language
        {
            get
            {
                if (_Language == "")
                {
                    _Language = GetAppSetting("Language");
                }
                return _Language;
            }
        }
        private static int _DefaultPageSize = 0;
        /// <summary>
        /// Tên ứng dụng
        /// </summary>
        public static int DefaultPageSize
        {
            get
            {
                if (_DefaultPageSize == 0)
                {
                    var s = GetAppSetting("DefaultPageSize");
                    if (string.IsNullOrWhiteSpace(s))
                        _DefaultPageSize = 0;
                    _DefaultPageSize = Convert.ToInt32(GetAppSetting("DefaultPageSize"));
                }
                return _DefaultPageSize;
            }
        }
        private static string _FilePath = "";
        /// <summary>
        /// Ngôn ngữ mặc định sử dụng trên ứng dụng
        /// </summary>
        public static string FilePath
        {
            get
            {
                if (_FilePath == "")
                {
                    _FilePath = GetAppSetting("FILE_PATH");
                }
                return _FilePath;
            }
        }
        private static string _FileRootUrl = "";
        /// <summary>
        /// Ngôn ngữ mặc định sử dụng trên ứng dụng
        /// </summary>
        public static string FileRootUrl
        {
            get
            {
                if (_FileRootUrl == "")
                {
                    _FileRootUrl = GetAppSetting("FileRootUrl");
                }
                return _FileRootUrl;
            }
        }
        public const string VIEW_ADD = "ADD";
        public const string VIEW_EDIT = "EDIT";
        private static string _MinigamePath = "";
        /// <summary>
        /// Ngôn ngữ mặc định sử dụng trên ứng dụng
        /// </summary>
        public static string MinigamePath
        {
            get
            {
                if (_MinigamePath == "")
                {
                    _MinigamePath = GetAppSetting("MINIGAME_PATH");
                }
                return _MinigamePath;
            }
        }
        private static string[] _TinyImgKeys = null;
        /// <summary>
        /// Ngôn ngữ mặc định sử dụng trên ứng dụng
        /// </summary>
        public static string[] TinyImgKeys
        {
            get
            {
                if (_TinyImgKeys == null)
                {
                    string keyString = GetAppSetting("TINYIMG_KEYS");
                    if (string.IsNullOrEmpty(keyString))
                        return null;
                    _TinyImgKeys = keyString.Split(';');
                }
                return _TinyImgKeys;
            }
        }
        #region CDN H5
        private static string _CDNHOST = "";
        /// <summary>
        /// Ngôn ngữ mặc định sử dụng trên ứng dụng
        /// </summary>
        public static string CdnHost
        {
            get
            {
                if (_CDNHOST == "")
                {
                    _CDNHOST = GetAppSetting("CdnHost");
                }
                return _CDNHOST;
            }
        }
        private static string _CDNUSER = "";
        /// <summary>
        /// Ngôn ngữ mặc định sử dụng trên ứng dụng
        /// </summary>
        public static string CdnUser
        {
            get
            {
                if (_CDNUSER == "")
                {
                    _CDNUSER = GetAppSetting("CdnUser");
                }
                return _CDNUSER;
            }
        }
        private static string _CDNPW = "";
        /// <summary>
        /// Ngôn ngữ mặc định sử dụng trên ứng dụng
        /// </summary>
        public static string CdnPw
        {
            get
            {
                if (_CDNPW == "")
                {
                    _CDNPW = GetAppSetting("CdnPw");
                }
                return _CDNPW;
            }
        }
        #endregion
    }
}