﻿using Bussiness;
using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace clonemxh.Controllers
{
    [Authorize]
    public class NotificationController : _BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult List(NotificationSearchingCondition model)
        {
            long rowCount = 0;
            model.Notifier_id = Userlogin();

            var list = NotificationService.ListNotification(model, out rowCount);
            foreach (var item in list)
            {

                item.CheckUserInGroup = GroupService.GetUserInGroup(item.Actor_Id, item.GroupJoin);
                item.CheckFriend = FriendSevice.CheckFriend(Userlogin(), item.Actor_Id);
            }
            NotificationPanginationResult notificationPanginationResult = new NotificationPanginationResult()
            {
                Page = model.Page,
                PageSize = model.PageSize,
                RowCount = rowCount,
                Data = list
            };
            return View("Notification", notificationPanginationResult);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Page"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public ActionResult ViewAll(int Page,int PageSize)
        {
            NotificationSearchingCondition model = new NotificationSearchingCondition();
            long rowCount = 0;
            model.Notifier_id = Userlogin();
            model.Page = Page;
            model.PageSize = PageSize;

            var list = NotificationService.ListNotification(model, out rowCount);
            foreach (var item in list)
            {

                item.CheckUserInGroup = GroupService.GetUserInGroup(item.Actor_Id, item.GroupJoin);
                item.CheckFriend = FriendSevice.CheckFriend(Userlogin(), item.Actor_Id);
            }
            NotificationPanginationResult notificationPanginationResult = new NotificationPanginationResult()
            {
                Page = model.Page,
                PageSize = model.PageSize,
                RowCount = rowCount,
                Data = list
            };
            return View(notificationPanginationResult);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Count()
        {
            int id = Userlogin();
            int count = 0;
            count = NotificationService.Count(id);
            ViewBag.NotificationCount = count;
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Page"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(int Page, int PageSize)
        {
            try
            {
                NotificationSearchingCondition model = new NotificationSearchingCondition();
                long rowCount = 0;
                model.Notifier_id = Userlogin();
                model.Page = Page;
                model.PageSize = PageSize;
                var list = NotificationService.ListNotification(model, out rowCount);
                foreach (var item in list)
                {
                    if (item.Status == 0)
                    {
                        NotificationService.Update(item.Notification_Id, 1);
                    }
                }
                return CreateSuccessResult("Thành công");
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateToReaded(int id)
        {
            try
            {
                NotificationService.Update(id, 2);
                return CreateSuccessResult("Thành công");
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        public ActionResult Get(int id)
        {
            try
            {
                if (id == 0)
                {
                    return View();
                }
                else
                {
                    NotificationResult data = NotificationService.GetNotification(id);
                    data.CheckUserInGroup = GroupService.GetUserInGroup(data.Actor_Id, data.GroupJoin);
                    data.CheckFriend = FriendSevice.CheckFriend(Userlogin(), data.Actor_Id);
                    return View(data);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
          
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult UpdateAll()
        {
            try
            {
                int notifierId = Userlogin();
                bool result = NotificationService.Update_All_Notification(notifierId);
        
                    return CreateSuccessResult("Thành công");
    
            }catch (Exception ex) {
                return CreateFailResult("Thất bại");
            }           
        }
    }
}