﻿using Bussiness;
using clonemxh.Helper;
using Core;
using Data;
using Microsoft.Security.Application;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using System.Xml.Linq;

namespace clonemxh.Controllers
{
    [Authorize]
    public class CommentController : _BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        /// 
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add(Comment data)
        {
            try
            {
               // data.Content = Sanitizer.GetSafeHtmlFragment(data.Content);
                data.UserId = Userlogin();
                if(data.PostID == 0)
                {
                   return CreateFailResult("Bài viết không có thật");
                }
                if (string.IsNullOrEmpty(data.Content))
                {
                   return CreateFailResult("Nội dung không được để trống");
                }
                data.CreateAt = DateTime.Now;
                int count = PostSevice.AddComment(data);
                int noti = Notification(count,0, EntityType.Comment);
                CallNotification();
                return CreateResult(count);
            }
            catch (Exception ex) {
                return CreateFailResult("Thất bại");
            }
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parentid"></param>
        /// <returns></returns>
        /// 
        [Authorize]
        public ActionResult GetContent(int id)
        {
            try
            {
                if (id == 0)
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Bài viết không có thật");
                    return View();
                }
                Comment comment = PostSevice.GetComment(id);
                return View(comment);
            }
            catch (Exception ex)
            {
                ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Thất bại");
                return View();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Get(int id)
        {
            try
            {
                if (id == 0)
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Bài viết không có thật");
                    return View();
                }
                Comment comment = PostSevice.GetComment(id);
                return View(comment);
            }
            catch (Exception ex)
            {
                ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Thất bại");
                return View();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ListComment(CommentSearchingCondition model)
        {
            long rowCount = 0;
            int roleid = 0;
            model.UserId = Userlogin();
            var list = PostSevice.ListComments(model, out rowCount,out roleid);

            var resultModel = new CommentPaginationResult()
            {
                Page = model.Page,
                RowCount = rowCount,
                PageSize = DefaultPagSize,
                Data = list,
                RoleID=roleid
            };
            return View(resultModel);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [Authorize]
        public ActionResult Delete(int id)
        {
            if (id == 0)
            {
                ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Bài Viết không có thật");
            }            
            Comment comment = PostSevice.GetComment(id);            
            return View(comment);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public ActionResult DeleteComment(int id)
        {
            bool result = false;
            result = PostSevice.DeleteComment(id);
            if (result)
            {
                CallNotification();
                return CreateSuccessResult("Thành công");
            }
            else
            {
                return CreateFailResult("Thất Bại");
            }
        }
        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Bài Viết không có thật");
            }
            Comment comment = PostSevice.GetComment(id);
            return View(comment);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Comment"></param>
        /// <returns></returns>
        /// 
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Update(Comment cmt)
        {
            bool result = false;
            try
            {
                cmt.Content = Sanitizer.GetSafeHtmlFragment(cmt.Content);
                result = PostSevice.UpdateComment(cmt);
                if (result)
                {
                    return CreateSuccessResult("Thành công");

                }
                else
                {
                    return CreateFailResult("Thất bại");
                }
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
    }
}