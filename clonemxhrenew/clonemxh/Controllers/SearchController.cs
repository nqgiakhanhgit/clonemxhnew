﻿using Bussiness;
using clonemxh.Helper;
using Core;
using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace clonemxh.Controllers
{
    [Authorize]
    public class SearchController : _BaseController
    {

        // GET: Search
        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        /// 
        [ValidateInput(false)]
        public ActionResult Search(string SearchText=null, int groupid=0)
        {
            SearchText = Sanitizer.GetSafeHtmlFragment(SearchText);
            ViewBag.SearchText = SearchText;
            ViewBag.GroupID =groupid;

            if(SearchText != null )
            {
                SearchCondition model = new SearchCondition()
                {
                    UserId = Userlogin(),
                    GroupID = groupid,
                    Page = 1,
                    PageSize = 3,
                    SearchText = SearchText
                };
                long rowCount = 0;
                var list = SearchService.SearchResult(model, out rowCount).ToList();
                List<PostEx> listEx = new List<PostEx>();
                foreach (var postItem in list)
                {
                    if (postItem.Type == "Post")
                    {
                        listEx.Add(new PostEx
                        {
                            PostID = postItem.PostID,
                            Avartar = postItem.Avatar,
                            Content = postItem.Content,
                            UserId = postItem.UserID,
                            GroupID = postItem.GroupID,
                            GroupName = postItem.GroupName,
                            UserFirstName = postItem.UserFirstName,
                            UserLastName = postItem.UserLastName,
                            PrivacyMode = postItem.PostPrivacyMode,
                            UserLiked = PostSevice.GetLike(postItem.PostID, Userlogin()),
                            UsersLikePost = postItem.LikeCount,
                            CommentCount = postItem.CommentCount,
                            Images = PostSevice.Images(postItem.PostID)
                        });
                    }
                }
                SearchResult searchResult = new SearchResult()
                {
                    Data = list,
                    Page = model.Page,
                    PageSize = model.PageSize,
                    Posts = listEx.ToList(),
                };

                return View(searchResult);
            }
            else
            {
                return View();
            }
        }
    }
}