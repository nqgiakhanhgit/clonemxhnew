﻿using Bussiness;
using clonemxh.Helper;
using Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Facebook;

namespace clonemxh.Controllers
{
    public class LoginFaceBookController : _BaseController
    {
        // GET: LoginFaceBook
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult LoginFaceBook()
        {
            var fb = new FacebookClient();
            var loginURL = fb.GetLoginUrl(new
            {
                client_id = ConfigurationManager.AppSettings["clientID"],

                redirect_uri = "http://clonemxh.uk.to/LoginFaceBook/FaceBookCallback/",
                response_type = "code",
                scope = "public_profile,email",
            });
            return Redirect(loginURL.AbsoluteUri);
        }

        public ActionResult FaceBookCallback(string code)
        {
            dynamic me = null;
            UserEntity CheckUser = null;
            var fb = new FacebookClient();
            dynamic result = fb.Post("oauth/access_token", new
            {
                client_id = ConfigurationManager.AppSettings["clientID"],
                client_secret = ConfigurationManager.AppSettings["secret"],
                redirect_uri = "http://clonemxh.uk.to/LoginFaceBook/FaceBookCallback/",
                code = code,


            });
            var accessToken = result.access_token;
            if (!string.IsNullOrEmpty(accessToken))
            {
                fb.AccessToken = accessToken;
                me = fb.Get("me?fields=first_name,middle_name,last_name,email");
                string email = me.email;
                string first_name = me.first_name;
                string last_name = me.last_name;
                string middle_name = me.middle_name;
                string phone = me.phone;
                string id = me.id;
                CheckUser = UserService.GetUserByEmail(email);
                if (CheckUser == null)
                {
                    UserEntity data = new UserEntity()
                    {
                        UserFirstName = first_name + middle_name,
                        UserLastName = last_name,
                        Email = email,
                        Phone = phone,
                        Password = id
                    };
                    UserService.Register(data);
                    UserService.Login(data.Email, data.Password);

                }
                else
                    if (CheckUser != null)
                {
                    UserEntity data = UserService.GetUserByEmail(email);
                    FormsAuthentication.SetAuthCookie(CookieHelper.UserToCookieString(data), false);
                }
                return Redirect("/");
            }
            return Redirect("/");
        }

    }
}
