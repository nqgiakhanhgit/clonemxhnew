﻿using Bussiness;
using Core;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace clonemxh.Controllers
{
    [Authorize]
    public class FollowController : _BaseController
    {
        // GET: Friend
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddFollow (int id)
        {           
            int res = 0;
            try
            {
                Follow f = new Follow()
                {
                    UserId2 = id,
                    UserId1 = Userlogin(),
                };
                res = FriendSevice.AddFollow(f);
                int noti = Notification(res, id, EntityType.Follow);
                CallNotification();
                return CreateSuccessResult("Thành công");                
            }
            catch(Exception ex) {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UnFollow(int id)
        {
            bool result = false;
            try
            {
                result = FriendSevice.UnFollow(Userlogin(), id);
                if (result)
                {
                    return CreateSuccessResult("Thành công");
                }
                else
                {
                    return CreateFailResult("Thất Bại");
                }
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        [HttpGet]
        public ActionResult ListFollow(UserSearchingCondition model)
        {
            ViewBag.View = 2;
            ViewBag.Title = "Đã theo dõi";
            long rowCount = 0;
            model.UserId = Userlogin();
            var list = new List<UserEntity>();
            list = FriendSevice.FollowList(model, out rowCount).ToList();
            var resultModel = new UserPaginationResult()
            {
                Page = model.Page,
                RowCount = rowCount,
                PageSize = model.PageSize,
                ListFollow = list
            };
            //Session[SESSION_GAMECATEGORY_SEARCHCONDITIONS] = model;
            return View(resultModel);
        }
        [HttpGet]
        public ActionResult ListFollower(UserSearchingCondition model)
        {
            ViewBag.Title = "Người theo dõi";
            ViewBag.View = 1;
            long rowCount = 0;
            model.UserId = Userlogin(); 
            var list = new List<UserEntity>();
            list = FriendSevice.FollowerList(model, out rowCount).ToList();
            var resultModel = new UserPaginationResult()
            {
                Page = model.Page,
                RowCount = rowCount,    
                PageSize = model.PageSize,
                ListFollow = list,
            };
            //Session[SESSION_GAMECATEGORY_SEARCHCONDITIONS] = model;
            return View(resultModel);
        }
    }
}