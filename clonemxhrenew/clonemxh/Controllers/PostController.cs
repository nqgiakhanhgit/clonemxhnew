﻿using Bussiness;
using clonemxh.Helper;
using Core;
using Data;
using Data.SQL;
using Microsoft.Ajax.Utilities;
using Microsoft.Security.Application;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using System.Web.Razor.Generator;

namespace clonemxh.Controllers
{
    public class PostController : _BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Add(int id = 0,int postType = 1)
        {
            PostEx post = new PostEx()
            {
                GroupID = id,
            };
            return View(post);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="post"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(PostEx post, IEnumerable<HttpPostedFileBase> image = null)
        {
            int count = 0;
            int imagecount = 0;

            List<Image> images = new List<Image>();
            try
            {
                post.Content = Sanitizer.GetSafeHtmlFragment(post.Content);
                if (string.IsNullOrEmpty(Sanitizer.GetSafeHtmlFragment(post.Content)) || string.IsNullOrWhiteSpace(Sanitizer.GetSafeHtmlFragment(post.Content)))
                {
                    return CreateFailResult("Nội dung không hợp lệ");
                }
                if (image == null && string.IsNullOrEmpty(post.Content))
                {
                    CreateFailResult("Thất bại");
                }
                else
                {
                    post.CreateAt = DateTime.Now;
                    count = PostSevice.AddPost(post);
                    post.PostID = count;
                    if (image != null)
                    {
                        foreach (var img in image)
                        {
                            if (IsImageFile(img))
                            {
                                Image imageadd = new Image()
                                {
                                    PostID = count,
                                    Url = UploadFile(img, "/Theme/Asset/Post/", count),
                                };
                                imagecount = PostSevice.AddImage(imageadd);
                                images.Add(new Image
                                {
                                    ImageID = imagecount,
                                    PostID = count,
                                    Url = imageadd.Url
                                });
                            }
                            else
                            {
                                return CreateFailResult("Tệp phải là ảnh");
                            }
                        }
                        post.Images = images;
                      
                    }
                    if (post.GroupID != 0)
                    {
                        int noti = Notification(count, 0, EntityType.Post);
                    }
                }
                return CreateSuccessResult("Thành công");
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        [ValidateInput(false)]
        public ActionResult ListPost(PostSearchingCondition model,int postType=0)
        {
            
            ViewBag.GroupID = model.GroupID;
            // Chỉ check khi vào profile page
            if(postType == PostType.ProfilePost)
            {
                if (model.UserId != Userlogin())
                {
                    if (FriendSevice.CheckFriend(Userlogin(), model.UserId).Status == 1)
                    {
                        model.PrivacyMode = 1;
                    }
                    else
                    {
                        model.PrivacyMode = 2;
                    }
                }
                else
                {
                    model.PrivacyMode = 0;
                }
            }
            var list = new List<PostEx>();
            long rowCount = 0;
            if (postType != 0)
            {
                 list = PostSevice.ListPost(model, out rowCount, postType);
            }
            else
            {
                if (string.IsNullOrEmpty(model.SearchText) && string.IsNullOrWhiteSpace(model.SearchText) && model.SearchText==null )
                {
                    list = new List<PostEx>();
                }
                else
                {
                   model.SearchText = Sanitizer.GetSafeHtmlFragment(model.SearchText);
                    list = SearchService.ListPost(model, out rowCount);
                   
                }   
            }        
            PostExinfo(list);           
            var resultModel = new PostPaginationResult()
            {
                Page = model.Page,
                RowCount = rowCount,
                PageSize = model.PageSize,
                ListPost = list
            };
            return View(resultModel);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <param name="postType"></param>
        /// <returns></returns>
        public ActionResult ListPostJson(int userid,int page,int pagesize,int postType = 4)
        {
            PostSearchingCondition model = new PostSearchingCondition()
            {
                Page = page,
                PageSize = pagesize,
                UserId = userid,
            };
            ViewBag.GroupID = model.GroupID;
            if (model.UserId != Userlogin())
            {
                if (FriendSevice.CheckFriend(Userlogin(), model.UserId).Status == 1)
                {
                    model.PrivacyMode = 1;
                }
                else
                {
                    model.PrivacyMode = 2;
                }
            }
            else
            {
                model.PrivacyMode = 0;
            }
            var list = new List<PostEx>();
            long rowCount = 0;
            if (postType != 0)
            {
                list = PostSevice.ListPost(model, out rowCount, postType);
            }
            else
            {
                if (string.IsNullOrEmpty(model.SearchText) && string.IsNullOrWhiteSpace(model.SearchText) && model.SearchText == null)
                {
                    list = new List<PostEx>();
                }
                else
                {
                    model.SearchText = Sanitizer.GetSafeHtmlFragment(model.SearchText);
                    list = SearchService.ListPost(model, out rowCount);

                }
            }
            PostExinfo(list);
            var resultModel = new PostPaginationResult()
            {
                Page = model.Page,
                RowCount = rowCount,
                PageSize = DefaultPagSize,
                ListPost = list
            };
            return CreateResult(resultModel);
        }
        /// <summary>
        /// Load lại block lúc Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Get(int id=0)
        {
            try
            {
                if (id == 0)
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Bài viết không có thật");
                    return View("PartialView/Post");
                }
                PostEx post = PostSevice.GetPost(id);
                post.UserLiked = PostSevice.GetLike(post.PostID, Userlogin());
                return View("PartialView/Post", post);
            }
            catch (Exception ex)
            {
                ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Thất bại");
                return View("PartialView/Post");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [Authorize]
        public ActionResult Edit(int id=0)
        {

            PostEx data = new PostEx();
            data = PostSevice.GetPost(id);
            return View(data);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="post"></param>
        /// <param name="image"></param>
        /// <param name="imagelistDelete"></param>
        /// <returns></returns>
        /// 
        [Authorize]
        [ValidateInput(false)]
        public ActionResult Update(Post post, IEnumerable<HttpPostedFileBase> image = null, IEnumerable<Image> imagelistDelete = null)
        {
            post.Content = Sanitizer.GetSafeHtmlFragment(post.Content);
      
            bool count = false;
            int imagecount = 0;
            List<Image> images = new List<Image>();
            try
            {
                if (string.IsNullOrEmpty(Sanitizer.GetSafeHtmlFragment(post.Content))|| string.IsNullOrWhiteSpace(Sanitizer.GetSafeHtmlFragment(post.Content)))
                {
                    return CreateFailResult("Nội dung không hợp lệ");
                }
                if (post.Content ==null || string.IsNullOrEmpty(post.Content)|| string.IsNullOrWhiteSpace(post.Content))
                {
                    return CreateFailResult("Nội dung không được để trống");
                }
                if (image == null && string.IsNullOrEmpty(post.Content))
                {
                    CreateFailResult("Thất bại");
                }
                else
                {
                    count = PostSevice.UpdatePost(post);
                    //Xóa ảnh
                    // Làm thế này giữ nguyên được file ảnh khi người dùng không submit
                    if (imagelistDelete != null)
                    {
                        foreach (var img in imagelistDelete)
                        {
                            DeleteImageFile(img.Url);
                            PostSevice.DeleteImage(img.ImageID);
                        }
                    }
                    //Thêm ảnh
                    if (image != null)
                    {
                        foreach (var img in image)
                        {
                            if (IsImageFile(img))
                            {
                                Image imageadd = new Image()
                                {
                                    PostID = post.PostID,
                                    Url = UploadFile(img, "/Theme/Asset/Post/", post.PostID),
                                };
                                imagecount = PostSevice.AddImage(imageadd);
                            }
                            else
                            {
                                return CreateFailResult("Tệp phải là ảnh");
                            }
                        }
                    }
                }
                return CreateSuccessResult("Thành công");
            }
            catch (Exception ex)
            {
                return CreateResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Delete(int id)
        {
            PostEx post = new PostEx();
            post = PostSevice.GetPost(id);
            if (post == null)
            {
                ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Bài viết không có thật");
                return View(post);
            }
            else
            {
                return View(post);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public ActionResult DeletePost(int id)
        {
            bool result = false;
            var imagelistDelete = PostSevice.Images(id);
            if (imagelistDelete.Count > 0)
            {
                foreach (var img in imagelistDelete)
                {
                    DeleteImageFile(img.Url);
                }
            }
            result = PostSevice.DeletePost(id);

            if (result)
            {
                CallNotification();
                return CreateSuccessResult("Thành công");
            }
            else
            {
                return CreateFailResult("Thất Bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        protected void PostExinfo(List<PostEx> list)
        {
            foreach (var item in list)
            {
                item.UserLiked = PostSevice.GetLike(item.PostID, Userlogin());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Detail(int id=0)
        {
            if (id == 0 )
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Bài viết không có thật");
                    return View();
                }
            PostEx post = PostSevice.GetPost(id);
            if(post == null)
            {
                return RedirectoNotFound();
            }
            if(post.PostID != 0) {
                post.UserLiked = PostSevice.GetLike(post.PostID, Userlogin()); 
                ViewBag.GroupID = post.GroupID;
            }

                return View( post);
        }
    }
}