﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.DynamicData;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using Bussiness;
using clonemxh.Helper;
using Core;
using Microsoft.Security.Application;

namespace clonemxh.Controllers
{
    public class UserController : _BaseController
    {
       /// <summary>
       /// 
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
        public ActionResult Index(int? id)
        {
            ViewBag.postType = 1;
            ViewBag.GroupID = 0;
            UserEx data = new UserEx();
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectoLogin();
            }
            if (id == 0 || string.IsNullOrEmpty(id.ToString()))
            {
                data = UserService.GetProfile(Userlogin());
                data.Friendcount = FriendSevice.FriendCount(Userlogin());
                data.FollowCount = FriendSevice.FolowCount(Userlogin());
                data.FollowerCount = FriendSevice.FollowerCount(Userlogin());
                data.BlockedCount = FriendSevice.BlockedCount(Userlogin());
            }
            else
            {
                data = UserService.GetProfile(id.Value);
                if(data.UserId != 0)
                {
                    data.FriendCheck = FriendSevice.CheckFriend(Userlogin(), id.Value);
                    data.Follow = FriendSevice.CheckFollow(Userlogin(), id.Value);
                    data.Block = FriendSevice.CheckBlock(Userlogin(), id.Value);
                    data.Friendcount = FriendSevice.FriendCount(id.Value);
                    data.FollowCount = FriendSevice.FolowCount(id.Value);
                    data.FollowerCount = FriendSevice.FollowerCount(id.Value);
                    if (data.Block.BlockID != 0)
                    {
                        return RedirectoNotFound();
                    }
                }
    
            }
            if (data.UserId != 0)
            {
                return View(data);
            }
            else
            {
                return RedirectoNotFound();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            UserEntity user = new UserEntity();
            return View(user);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult Login(string email, string password)
        {
            UserEntity oldvalue = new UserEntity();
            oldvalue = new UserEntity()
            {
                Email = email
            };
            try
            {
                var user = UserService.Login(email, UserHelper.HashPassword(password));   
                if (user.UserId == 0)
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Sai tài khoảng hoặc mật khẩu");

                    return View("Login",oldvalue);
                }
                FormsAuthentication.SetAuthCookie(CookieHelper.UserToCookieString(user), false);
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                ViewData["ActionResult"] = ApiResult.CreateInsuccessResult(ex.Message);
                return View("Login",oldvalue);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            UserEntity user = new UserEntity();
            return View(user);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Register(UserEntity user)
        {
            try
            {
                if (Sanitizer.GetSafeHtmlFragment(user.UserFirstName) == "" ||
                    Sanitizer.GetSafeHtmlFragment(user.UserLastName) == "" ||
                    Sanitizer.GetSafeHtmlFragment(user.Phone) == "")
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Giá trị không hợp lệ");
                    return View(user);
                }
                if (string.IsNullOrEmpty(user.UserFirstName) || string.IsNullOrWhiteSpace(user.UserFirstName))
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Họ và tên đệm không được để trống");
                    return View(user);
                }
                if (string.IsNullOrEmpty(user.UserLastName) || string.IsNullOrWhiteSpace(user.UserLastName))
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Tên không được để trống");
                    return View(user);
                }
                if (string.IsNullOrEmpty(user.Email) || string.IsNullOrWhiteSpace(user.Email))
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Email không được để trống");
                    return View(user);
                }
                if (UserHelper.IsValidEmail(user.Email) == false)
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Email không được hỗ trợ");
                    return View(user);
                }
                if (string.IsNullOrEmpty(user.Password) || string.IsNullOrWhiteSpace(user.Password))
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Mật khẩu không được để trống");
                    return View(user);
                }
                if (UserHelper.ContainsSpecialCharacter(user.Password))
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Mật khẩu không được chứa kí tự đặc biệt");
                    return View(user);
                }
                if (user.RePass != user.Password)
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Mật khẩu không trùng khớp");
                    return View(user);
                }
                if (!UserHelper.IsSqlDateTime(user.BirthDay))
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Ngày sinh không hợp lệ");
                    return View(user);
                }
                if (string.IsNullOrEmpty(user.Phone) || string.IsNullOrWhiteSpace(user.Phone))
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Số điện thoại không được để trống");
                    return View(user);
                }
                if (UserHelper.IsValidPhoneNumber(user.Phone) == false)
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Số điện thoại không hợp lệ");
                    return View(user);
                }
                if (UserService.GetUserByEmail(user.Email).UserId != 0)
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Tài khoảng đã tồn tại");
                    return View(user);
                }
                user.UserFirstName = Sanitizer.GetSafeHtmlFragment(user.UserFirstName);
                user.UserLastName = Sanitizer.GetSafeHtmlFragment(user.UserLastName);
                user.Phone = Sanitizer.GetSafeHtmlFragment(user.Phone);
                user.Password = Sanitizer.GetSafeHtmlFragment(user.Password);
                user.Email = Sanitizer.GetSafeHtmlFragment(user.Email);
                UserEntity regiter = new UserEntity()
                {
                    UserFirstName = user.UserFirstName,
                    UserLastName = user.UserLastName,
                    Phone = user.Phone,
                    Password = UserHelper.HashPassword(user.Password),
                    Email = user.Email,
                    BirthDay = Convert.ToDateTime(user.BirthDay.ToString("yyyy-MM-dd HH:mm:ss.fff")),
                    Sex = user.Sex,
                    CreateDay = DateTime.Now
                };
                var res = UserService.Register(regiter);                
                UserEntity login = UserService.Login(regiter.Email,regiter.Password);
                FormsAuthentication.SetAuthCookie(CookieHelper.UserToCookieString(login), false);
                return RedirectToAction("Index", "Home");               
            }
            catch (Exception ex)
            {
                ViewData["ActionResult"] = ApiResult.CreateInsuccessResult(ex.Message);
                return View("Register", user);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "User");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Edit()
        {
            UserEntity user = new UserEntity();
            user = UserService.GetProfile(Userlogin());
            if (user.UserId != 0)
            {
                return View(user);
            }
            else
                return View(user);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="ava"></param>
        /// <param name="wall"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public  ActionResult Update(UserEntity user, string birthday)
        {
            user.UserId = Userlogin();
            user.UserFirstName = Sanitizer.GetSafeHtmlFragment(user.UserFirstName);
            user.UserLastName = Sanitizer.GetSafeHtmlFragment(user.UserLastName);
            user.Phone = Sanitizer.GetSafeHtmlFragment(user.Phone);
            user.Education = Sanitizer.GetSafeHtmlFragment(user.Education);
            user.Skill = Sanitizer.GetSafeHtmlFragment(user.Skill);
            user.Note = Sanitizer.GetSafeHtmlFragment(user.Note);
            user.Location = Sanitizer.GetSafeHtmlFragment(user.Location);
            // Chuyển đổi chuỗi thành kiểu DateTime
            DateTime birthdayDateTime;
            if (DateTime.TryParseExact(birthday, "yyyy/MM/dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthdayDateTime))
            {
                // Gán giá trị DateTime cho thuộc tính BirthDay của đối tượng user
                user.BirthDay = birthdayDateTime;
            }
            try
            {
                if (Sanitizer.GetSafeHtmlFragment(user.UserFirstName) == "" ||
                     Sanitizer.GetSafeHtmlFragment(user.UserLastName) == "" ||
                     Sanitizer.GetSafeHtmlFragment(user.Phone) == "" ||
                     Sanitizer.GetSafeHtmlFragment(user.Education) == "" ||
                     Sanitizer.GetSafeHtmlFragment(user.Skill) == "" ||
                     Sanitizer.GetSafeHtmlFragment(user.Note) == "" ||
                     Sanitizer.GetSafeHtmlFragment(user.Location) == "")
                {
                    return CreateFailResult("Giá trị không hợp lệ");
                }
                if (string.IsNullOrEmpty(user.UserFirstName) || string.IsNullOrWhiteSpace(user.UserFirstName))
                {
                    return CreateFailResult("Họ và tên đệm không được để trống");
                }
                if (string.IsNullOrEmpty(user.UserLastName) || string.IsNullOrWhiteSpace(user.UserLastName))
                {
                    return CreateFailResult("Tên không được để trống");
                }
                if (string.IsNullOrEmpty(user.BirthDay.ToString()) ||!UserHelper.IsSqlDateTime(user.BirthDay))
                {
                    return CreateFailResult("Ngày sinh không hợp lệ");
                }
                if (string.IsNullOrEmpty(user.Phone) || string.IsNullOrWhiteSpace(user.Phone))
                {
                    return CreateFailResult("Số điện thoại không được để trống");
                }
                var res = UserService.UpdateProfile(user);
                if (res == true)
                {
                    return CreateSuccessResult("Thành Công");
                }
                else
                {
                    return CreateFailResult("Thất bại");
                }

            }
            catch (Exception ex)
            {
                return CreateFailResult(ex.Message);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult EditAva(int id)
        {
            UserEntity user = new UserEntity();
            user.UserId = Userlogin();
            user.Avartar = UserService.GetImage(id, ImageType.Avatar);
            return View(user);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult EditWallPaper(int id)
        {
            UserEntity user = new UserEntity();
            user.UserId = Userlogin();
            user.WallPaper = UserService.GetImage(id, ImageType.Wall);
            return View(user);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ava"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult UpdateAvatar(string Avartar,HttpPostedFileBase ava = null)
        {
            bool result = false;
            try
            {
                if (ava != null && ava.ContentLength > 0)
                {
                    if(IsImageFile(ava)) {
                        if (Avartar != DefaulImage)
                        {
                            DeleteImageFile(Avartar);
                        }
                    }
                    else
                    {
                        return CreateFailResult("Tệp phải là ảnh");
                    }
                    string newAvatar = UploadFile(ava, "/Theme/Asset/Ava/", Userlogin());
                    result = UserService.UpdateImage(Userlogin(), newAvatar, ImageType.Avatar);
                  return  CreateSuccessResult("Thành công");
                }
                else
                {
                    if (Avartar != null)
                    {
                        result = UserService.UpdateImage(Userlogin(), Avartar, ImageType.Avatar);
                        return CreateSuccessResult("Thành công");
                    }
                    else
                    {
                        return CreateFailResult("Thất  bại");
                    }             
                }
            }
            catch(Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
      
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="wall"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult UpdateWallPaper( string WallPaper, HttpPostedFileBase wall = null)
        {
            bool result = false;
            try
            {
                if (wall != null && wall.ContentLength >0)
                {
                    if (IsImageFile(wall))
                    {
                        if (WallPaper != DefaulImage)
                        {
                            DeleteImageFile(WallPaper);
                        }
                    }
                    else
                    {
                        return CreateFailResult("Tệp phải là ảnh");
                    }
                    string newAvatar = UploadFile(wall, "/Theme/Asset/Wall/", Userlogin());
                    result = UserService.UpdateImage(Userlogin(), newAvatar, ImageType.Wall);
                    return CreateSuccessResult("Thành công");
                }
                else
                {
                    if (WallPaper != null)
                    {
                        result = UserService.UpdateImage(Userlogin(), WallPaper, ImageType.Wall);
                        return CreateSuccessResult("Thành công");
                    }
                    else
                    {
                        return CreateFailResult("Thất  bại");
                    }
                }
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }

        }
    }
}