﻿using Bussiness;
using clonemxh.Helper;
using Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace clonemxh.Controllers
{
    public class _BaseController : Controller
    {
        // GET: _Base
        /// <summary>
        /// tải ảnh vào thư mục
        /// </summary>
        /// <param name="fileimage"></param>
        /// <param name="folder"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        public string DefaulImage = "/Theme/Asset/Ava/avatar-trang-4.jpg";
        /// <summary>
        /// 
        /// </summary>
        public int DefaultPagSize = 3;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileimage"></param>
        /// <param name="folder"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public string UploadFile(HttpPostedFileBase fileimage, string folder, int id)
        {
            string savepath;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            var fileName = Path.GetFileName(fileimage.FileName);
 
          
                var path = Path.Combine(Server.MapPath(folder), id.ToString() + fileName);
                fileimage.SaveAs(path);
                savepath = Url.Content(folder + id.ToString() + fileName);
                return  savepath;
        }
        /// <summary>
        /// Kiểm tra file có phải là ảnh không 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public bool IsImageFile(HttpPostedFileBase file)
        {
            var fileName = Path.GetFileName(file.FileName);
            string fileExtension = Path.GetExtension(fileName);
            string[] allowedExtensions = { ".jpg", ".jpeg", ".png", ".gif" };
            return allowedExtensions.Contains(fileExtension.ToLower());
        }
        /// <summary>
        /// Xóa ảnh
        /// </summary>
        /// <param name="filepath"></param>
        public void DeleteImageFile(string filepath)
        {
            if (Uri.IsWellFormedUriString(filepath, UriKind.Absolute))
            {
                // Nếu là 1 đường dẫn Uri không phải từ web thì không làm gì cả ..
                // Kiểm tra nếu filepath là một URL hợp lệ trong ứng dụng web của bạn
                // Ví dụ: Kiểm tra nếu URL bắt đầu với 'http://' hoặc 'https://'
            }
            else
            {
                if (!string.IsNullOrEmpty(filepath))
                {
                  var oldfile = Server.MapPath(filepath);
                  if (System.IO.File.Exists(oldfile))
                  {
                    System.IO.File.Delete(oldfile);
                  }
                }
            }
        }
        /// <summary>
        /// Id của user đang đăng nhập
        /// </summary>
        /// <returns></returns>
        protected int Userlogin()
        {
            if (User.Identity.IsAuthenticated)
            {
                return CookieHelper.CookieToStringUsers(User.Identity.Name).UserId;
            }
            else { return 0; }
        }
        /// <summary>
        /// Trả về trang 404
        /// </summary>
        /// <returns></returns>
        protected ActionResult RedirectoNotFound()
        {
            return RedirectToAction("NotFound", "Home");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        protected JsonResult CreateSuccessResult(string msg = "Thành công!")
        {
            return Json(new JsonResponseResult
            {
                Code = 1,
                Msg = msg,
                Data = null
            }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        protected JsonResult CreateResult(object data)
        {
            return Json(new JsonResponseResult
            {
                Code = 1,
                Msg = "Data",
                Data = data
            }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        protected JsonResult CreateFailResult(string msg = "Thất bại!")
        {
            return Json(new JsonResponseResult
            {
                Code = -1,
                Msg = msg,
                Data = null
            }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Trả về trang đăng nhập
        /// </summary>
        /// <returns></returns>
        protected ActionResult RedirectoLogin()
        {
           return RedirectToAction("Login", "User");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="notificatorid"></param>
        /// <param name="entity_type_id"></param>
        /// <returns></returns>
        public int Notification(int id,int notificatorid,int entity_type_id)
        {
            NotificationInsert noti = new NotificationInsert()
            {
                Entity_Id = id,
                Actor_Id = Userlogin(),
                Entity_Type_Id = entity_type_id,
                Notitfier_Id = notificatorid,
                Status = 0
            };
            int Notification = NotificationService.AddNotification(noti);
            return Notification;
        }
        /// <summary>
        /// 
        /// </summary>
        public void CallNotification()
        {
            Notificationhub.SendNotification();
        }
    }
    public class JsonResponseResult
    {
        public int Code { get; set; }
        public string Msg { get; set; }
        public object Data { get; set; }
    }
}