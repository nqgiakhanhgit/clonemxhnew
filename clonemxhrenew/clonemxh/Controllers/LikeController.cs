﻿using Bussiness;
using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Web;
using System.Web.Mvc;

namespace clonemxh.Controllers
{
    public class LikeController : _BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]     
        public ActionResult Add(Like data)
        {
            try
            {
                int count = 0;
                data.UserId = Userlogin();
                count = PostSevice.LikePost(data);
                PostEx post = PostSevice.GetPost(data.PostID);
           
                int noti = Notification(count, post.UserId, EntityType.LikePost);
                CallNotification();
                return CreateSuccessResult("Thành công");
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [Authorize]
        [HttpPost]
        public ActionResult UnLike(int PostID) {
            bool result = false;
            int userid = Userlogin();
            result = PostSevice.Unlike(PostID, userid);
            if(result)
            {            
                return CreateSuccessResult("Thành công");
            }
            else
            return CreateFailResult("Thất Bại");
        }
        public ActionResult ListUserLiked(int id)
        {
            List<UserEntity> list = new List<UserEntity>();
            list = PostSevice.UsersLikedPost(id);
            return View(list);
        }
    }
}