﻿using Bussiness;
using Core;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace clonemxh.Controllers
{
    [Authorize]
    public class BlockController : _BaseController
    {
        // GET: Friend
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Chặn người dùng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddBlock(int id)
        {           
            int res = 0;
            try
            {
                Block bl = new Block()
                {
                    UserId2 = id,
                    UserId1 = Userlogin(),
                };
                res = FriendSevice.AddBlock(bl);
                return CreateSuccessResult("Thành công");                
            }
            catch(Exception ex) {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// Bỏ chặn
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        public ActionResult BlockNotification(int id)
        {
            UserEntity user = UserService.GetProfile(id);
            return View(user);
        }
        [HttpPost]
        public ActionResult UnBlock(int id)
        {
            bool result = false;
            try
            {
                result = FriendSevice.UnBlock(Userlogin(), id);
                if (result)
                {
                    return CreateSuccessResult("Thành công");
                }
                else
                {
                    return CreateFailResult("Thất Bại");
                }
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult BlockList(UserSearchingCondition model)
        {
            ViewBag.View = 3;
            ViewBag.Title = "Đã chặn";
            long rowCount = 0;
            var list = FriendSevice.BlockList(model, out rowCount).ToList();
            var resultModel = new UserPaginationResult()
            {
                Page = model.Page,
                RowCount = rowCount,
                PageSize = model.PageSize,
                // Sử dụng chung 1 class Search condition
                ListFollow = list
            };
            //Session[SESSION_GAMECATEGORY_SEARCHCONDITIONS] = model;
            return View(resultModel);
        }
    }
}