﻿using Bussiness;
using Core;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.DynamicData;
using System.Web.Mvc;

namespace clonemxh.Controllers
{
    [Authorize]
    public class UserInGroupController : _BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userInGroup"></param>
        /// <returns></returns>
        ///    
        public ActionResult Add(UserInGroup userInGroup)
        {
            int count = 0;

            try
            {
                UserInGroup add = new UserInGroup()
                {
                    UserInGroupId = 0,
                    UserId = Userlogin(),
                    GroupID = userInGroup.GroupID,
                    RoleID = 3,
                    Status = userInGroup.Status,
                };
                count = GroupService.AddUserInGroup(add);
                CallNotification();
                int noti = Notification(count, 0, EntityType.JoinGroupRequest);
                return CreateSuccessResult("Thành công");
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult LeftOut(int id, int userid, int superadmin)
        {
            if (userid == superadmin)
            {
                ViewData["Content"] = "Vui lòng chọn Quản trị viên cao nhất !";
                ViewBag.id = id;
                ViewBag.userid = userid;
                ViewBag.superadmin = superadmin;
            }
            else
            {
                ViewData["Content"] = "Xác nhận Rời khỏi nhóm ?";
                ViewData["ButtonContent"] = "Rời Khỏi";
                ViewBag.id = id;
                ViewBag.userid = userid;
            }
            if (id != 0)
            {
                return View();
            }
            else
            {
                return RedirectoNotFound();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public ActionResult Kick(int id, int userid)
        {
            ViewData["Content"] = "Xác nhận xóa khỏi nhóm ?";
            ViewData["ButtonContent"] = "Xác nhận";
            ViewBag.id = id;
            ViewBag.userid = userid;
            if (id != 0)
            {
                return View("LeftOut");
            }
            else
            {
                return RedirectoNotFound();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="GroupID"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OutGroup(int GroupID, int userid)
        {
            bool result = false;
            result = GroupService.DeleteUserInGroup(userid, GroupID);
            if (result)
            {
                return CreateSuccessResult("Thành công");
            }
            else
                return CreateFailResult("Thất Bại");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        public ActionResult ListTable(int id)
        {
            ViewData["GroupID"] = id;
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ListUserInGroup(UserInGroupSearchingCondition model)
        {
            UserInGroup ug = new UserInGroup();
            ug = GroupService.GetUserInGroup(Userlogin(), model.GroupID);
            ViewData["UserInGroupCheck"] = ug;
            long rowCount = 0;
            var list = GroupService.UserInGroupList(model, out rowCount);
            var resultModel = new UserInGroupPaginationResult()
            {
                Page = model.Page,
                RowCount = rowCount,
                PageSize = DefaultPagSize,
                Data = list
            };
            return View(resultModel);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult Update(UserInGroup model)
        {
            bool result = false;
            try
            {
                result = GroupService.UpdateUserInGroup(model);
                if (result)
                {
                    UserInGroup ug = GroupService.GetUserInGroup(model.UserId,model.GroupID);
                    int noti = Notification(ug.UserInGroupId, model.UserId, EntityType.JoinGroupAcept);
                    CallNotification();
                    return CreateSuccessResult("Thành công");
                }
                else
                {
                    return CreateFailResult("Thất Bại");
                }
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="groupid"></param>
        /// <returns></returns>
        public ActionResult UpdateSuperAdmin(int userid, int groupid)
        {
            bool result = false;
            try
            {
                result = GroupService.UpdateSuperAdmin(userid, groupid);
                if (result)
                {
                    return CreateSuccessResult("Thành công");
                }
                else
                {
                    return CreateFailResult("Thất Bại");
                }
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
    }

}