﻿using Bussiness;
using clonemxh.Helper;
using Core;
using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace clonemxh.Controllers
{
    [Authorize]
    public class GroupController : _BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult SideBarGroupAdd()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SideBarGroup(int id = 0)
        {
            long rowCount = 0;
            GroupSearchingCondition model = new GroupSearchingCondition()
            {
                Page = 1,
                PageSize = 3,
                UserId = Userlogin(),
            };
            var list = GroupService.GroupJoinedList(model, out rowCount);
            var resultModel = new GroupPaginationResult()
            {
                Page = model.Page,
                RowCount = rowCount,
                PageSize = model.PageSize,
                Data = list
            };
            ViewData["GroupJoined"] = resultModel;
            var group = GroupService.Get(id);
            GroupEx groupex = new GroupEx()
            {
                GroupName = group.GroupName,
                GroupID = group.GroupID,
                GrouPicture = group.GrouPicture,
                UserInGroup = GroupService.GetUserInGroup(Userlogin(), id),
                UserId = group.UserId,
                UserFirstName = group.UserFirstName,
                UserLastName = group.UserLastName,
                Avartar = group.Avartar,
                SuperAdmin = group.SuperAdmin,
            };
            return View(groupex);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        /// 
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Add(GroupMXH group, HttpPostedFileBase wall = null)
        {
            int count = 0;
            try
            {
                group.GroupName = Sanitizer.GetSafeHtmlFragment(group.GroupName);
                group.SuperAdmin = Userlogin();
                if (string.IsNullOrEmpty(group.GroupName) || string.IsNullOrWhiteSpace(group.GroupName))
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Tên nhóm không được để trống");
                    return View();
                }
                if (wall != null && wall.ContentLength > 0)
                {
                    if (IsImageFile(wall))
                    {
                        if (group.GrouPicture != DefaulImage)
                        {
                            DeleteImageFile(group.GrouPicture);
                        }
                        group.GrouPicture = UploadFile(wall, "/Theme/Asset/GroupWall/", group.GroupID);
                    }
                    else
                    {
                        ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Tệp phải là ảnh");
                        return View();
                    }
                }
                // Fix => Count này của useringroup rồi .
                count = GroupService.AddGroup(group);
                UserInGroup userInGroup = new UserInGroup()
                {
                    UserId = Userlogin(),
                    GroupID = count,
                    Status=2,
                    RoleID=1
                };
                int addAdminGroup = GroupService.AddUserInGroup(userInGroup);
                if (count > 0)
                {
                    ViewData["ActionResult"] = ApiResult.CreateSuccessResult("Thành công");
                    return RedirectToAction("Get", "Group", new { id = count });
                }
                else
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Thất bại");
                    return View();
                }

            }
            catch (Exception ex)
            {
                ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Thất bại");
                return View();
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <param name="wall"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create(GroupMXH group, HttpPostedFileBase wall = null)
        {
            int count = 0;
            try
            {
                group.GroupName = Sanitizer.GetSafeHtmlFragment(group.GroupName);
                group.SuperAdmin = Userlogin();
                if (string.IsNullOrEmpty(group.GroupName) || string.IsNullOrWhiteSpace(group.GroupName))
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Tên nhóm không được để trống");
                    return View();
                }
                if (wall != null && wall.ContentLength > 0)
                {
                    if (IsImageFile(wall))
                    {
                        if (group.GrouPicture != DefaulImage)
                        {
                            DeleteImageFile(group.GrouPicture);
                        }
                        group.GrouPicture = UploadFile(wall, "/Theme/Asset/GroupWall/", group.GroupID);
                    }
                    else
                    {
                        ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Tệp phải là ảnh");
                        return View();
                    }
                }
                // Fix => Count này của useringroup rồi .
                count = GroupService.AddGroup(group);
                UserInGroup userInGroup = new UserInGroup()
                {
                    UserId = Userlogin(),
                    GroupID = count,
                    Status = 2,
                    RoleID = 1
                };
                int addAdminGroup = GroupService.AddUserInGroup(userInGroup);
                if (count > 0)
                {
                    ViewData["ActionResult"] = ApiResult.CreateSuccessResult("Thành công");
                    return RedirectToAction("Get", "Group", new { id = count });
                }
                else
                {
                    ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Thất bại");
                    return View();
                }

            }
            catch (Exception ex)
            {
                ViewData["ActionResult"] = ApiResult.CreateInsuccessResult("Thất bại");
                return View();
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Get(int id=0)
        {
            if(id == 0)
            {
                return RedirectoNotFound();
            }
            else
            {
                ViewBag.postType = 2;
                ViewBag.GroupID = id;
                ViewData["Check"] = GroupService.GetUserInGroup(Userlogin(), id);
                var group = GroupService.Get(id);
                GroupEx groupex = new GroupEx()
                {
                    GroupName = group.GroupName,
                    GroupID = group.GroupID,
                    GrouPicture = group.GrouPicture,
                    UserInGroup = GroupService.GetUserInGroup(Userlogin(), id),
                    UserId = group.UserId,
                    UserFirstName = group.UserFirstName,
                    UserLastName = group.UserLastName,
                    Avartar = group.Avartar,
                    SuperAdmin = group.SuperAdmin,
                };
                return View(groupex);
            }
          
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id =0)
        {
            if(id != 0)
            {
                GroupMXH gr = GroupService.Get(id);
                return View(gr);
            }
            else
            {
                return RedirectoNotFound();
            }
    
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult Update(GroupMXH group, HttpPostedFileBase groupPic)
        {
            bool result = false;
            try
            {
                if (string.IsNullOrEmpty(group.GroupName))
                {
                    return CreateFailResult("Tên nhóm không được để trống");
                }
                if (groupPic != null && groupPic.ContentLength > 0)
                {
                    if (IsImageFile(groupPic))
                    {
                        if (group.GrouPicture != DefaulImage)
                        {
                            DeleteImageFile(group.GrouPicture);

                        }
                        group.GrouPicture = UploadFile(groupPic, "/Theme/Asset/GroupWall/", group.GroupID);
                    }
                    else
                    {
                        return CreateFailResult("Tệp phải là ảnh");
                    }
                }
                result = GroupService.UpdateGroup(group);

                return CreateSuccessResult("Thành công");
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult DeleteGroup(int id)
        {
            return CreateFailResult();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GroupJoinedList(GroupSearchingCondition model)
        {
            long rowCount = 0;
            model.UserId = Userlogin();
            var list = GroupService.GroupJoinedList(model, out rowCount);
            var resultModel = new GroupPaginationResult()
            {
                Page = model.Page,
                RowCount = rowCount,
                PageSize = DefaultPagSize,
                Data = list
            };
            return View(resultModel);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult GroupJoined()
        {
            GroupSearchingCondition model = new GroupSearchingCondition();
            long rowCount = 0;
            model.UserId = Userlogin();
            model.Page = 0;
            var list = GroupService.GroupJoinedList(model, out rowCount);
            var resultModel = new GroupPaginationResult()
            {
                Page = model.Page,
                RowCount = rowCount,
                Data = list
            };
            return View(resultModel);
        }
    }
}