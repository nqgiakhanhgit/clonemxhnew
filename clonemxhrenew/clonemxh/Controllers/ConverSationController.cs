﻿using Bussiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace clonemxh.Controllers
{
    public class ConverSationController : _BaseController
    {
        // GET: ConverSation
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Store(string[] user)
        {
            try
            {                
                string creator = Userlogin().ToString();
                user = user.Concat(new string[] { creator }).ToArray();
                string result = string.Join(", ", user);
                int conversationId = 0;
              int count = MessageServices.Add(Userlogin(),result, out conversationId);
                return CreateResult(conversationId);
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }

    }
}