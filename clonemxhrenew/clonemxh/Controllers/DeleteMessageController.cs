﻿using Bussiness;
using Core;
using Core.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace clonemxh.Controllers
{
    public class DeleteMessageController : _BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conversationid"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult DeleteConveSation(int id)
        {
            try
            {
                int userid = Userlogin();
                int Delete = MessageServices.DeleteConverSation(id, userid);
                return CreateSuccessResult("Thành công");
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conversationid"></param>
        /// <param name="messageid"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult DeleteMessageOnly(int id)
        {
            try
            {
                int userid = Userlogin();
                int delete = MessageServices.DeleteMessageOnly(id, userid);
                return CreateSuccessResult("Thành công");
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        public ActionResult DeleteMessage(int id)
        {
            try
            {
                int userid = Userlogin();
                bool result = MessageServices.DeleteMessage(id);
                MessageHub.DeleteMess(id);
                return CreateSuccessResult("Thành công");
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conversationid"></param>
        /// <returns></returns>
        public ActionResult GetDeleteConvesation(int id)
        {
            ViewData["IDConverSation"] = id;
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageid"></param>
        /// <returns></returns>
        public ActionResult GetDeleteMessageOnly(int id)
        {
            ViewData["IDMess"] = id;
            return View();
        }
        public ActionResult GetDeleteMessage(int id)
        {
            ViewData["IDMess"] = id;
            return View();
        }
    }
}