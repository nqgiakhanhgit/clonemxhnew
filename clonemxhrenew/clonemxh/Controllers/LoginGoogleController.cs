﻿using Bussiness;
using clonemxh.Helper;
using Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace clonemxh.Controllers
{
    public class LoginGoogleController : _BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult LoginGoogle()
        {
            string clientID = ConfigurationManager.AppSettings["google:clientID"];
            string RedirectUri = "http://clonemxh.uk.to/LoginGoogle/GoogleCallBack";
            var url = $"https://accounts.google.com/o/oauth2/auth?client_id={clientID}&redirect_uri={RedirectUri}&response_type=code&scope=email%20profile";
            return Redirect(url);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<ActionResult> GoogleCallBack(string code)
        {
            UserEntity data = null;
            string tokenUrl = "https://oauth2.googleapis.com/token";
            string clientID = ConfigurationManager.AppSettings["google:clientID"];
            string clientscret = ConfigurationManager.AppSettings["google:clientscret"];
            string RedirectUri = "http://clonemxh.uk.to/LoginGoogle/GoogleCallBack";

            var content = new FormUrlEncodedContent(new[]
       {
            new KeyValuePair<string, string>("code", code),
            new KeyValuePair<string, string>("client_id", clientID),
            new KeyValuePair<string, string>("client_secret", clientscret),
            new KeyValuePair<string, string>("redirect_uri", RedirectUri),
            new KeyValuePair<string, string>("grant_type", "authorization_code")
        });
            // Lấy Acess Token
            HttpClient httpClient = new HttpClient();
            var response = await httpClient.PostAsync(tokenUrl, content);
            var tokenResponse = await response.Content.ReadAsStringAsync();
            var accessToken = ExtractUserInfo(tokenResponse);

            // Lấy thông tin người dùng
            var userUrl = "https://www.googleapis.com/oauth2/v2/userinfo";
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken["access_token"]);

            var res = await httpClient.GetAsync(userUrl);
            var userResponse = await res.Content.ReadAsStringAsync();
            var userinf = ExtractUserInfo(userResponse);
            DateTime birthDay = DateTime.Now;
            data = new UserEntity()
            {
                UserId = 0,
                UserFirstName = Convert.ToString(userinf["family_name"]),
                UserLastName = Convert.ToString(userinf["given_name"]),
                Email = Convert.ToString(userinf["email"]),
                Password = Convert.ToString(userinf["id"]),
                Phone = "",
                Avartar = Convert.ToString(userinf["picture"]),
                BirthDay = Convert.ToDateTime(birthDay.ToString("yyyy-MM-dd HH:mm:ss.fff")),
                CreateDay = DateTime.Now,
            };
            ////bool result = false;
            UserEntity check = UserService.GetUserByEmail(userinf["email"]);
            if (check.UserId == 0)
            {
                int count = UserService.Register(data);
                data = UserService.Login(data.Email, data.Password);
                FormsAuthentication.SetAuthCookie(CookieHelper.UserToCookieString(data), false);
                return Redirect("/");
            }
            else
            {
                data = UserService.GetUserByEmail(data.Email);
                //data= UserService.Autthorize(data.userEmail, data.userPassword);`
                FormsAuthentication.SetAuthCookie(CookieHelper.UserToCookieString(data), false);
                return Redirect("/");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public System.Collections.Generic.Dictionary<string, string> ExtractUserInfo(string jsonString)
        {
            var userInfo = new System.Collections.Generic.Dictionary<string, string>();

            // Xóa các ký tự không cần thiết như dấu \n và khoảng trắng
            jsonString = jsonString.Replace("\n", "").Replace("\r", "").Replace(" ", "");

            // Sử dụng Regex để trích xuất thông tin từ chuỗi JSON
            var matches = Regex.Matches(jsonString, "\"(.*?)\":\"(.*?)\"");

            foreach (Match match in matches)
            {
                userInfo[match.Groups[1].Value] = match.Groups[2].Value;
            }

            return userInfo;
        }
    }
}