﻿using Bussiness;
using Core;
using Core.Message;
using Microsoft.Security.Application;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;

namespace clonemxh.Controllers
{
    [Authorize]
    public class MessageController : _BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: Message
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult MessSideBar()
        {
            int userid = Userlogin();
            List<ConverSationResult> list = new List<ConverSationResult>();
            list = MessageServices.ListConverSation(userid);
            return View(list);
        }
        [HttpPost]
        public ActionResult ContactList()
        {
            int userid = Userlogin();
            List<ConverSationResult> list = new List<ConverSationResult>();
            list = MessageServices.ListConverSation(userid);
            return View(list);
        }
        /// <summary>
        /// conversationId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult MesssageList(MessageSearchingCondition model)
        {
            long rowCount = 0;
            string Name = "";
            model.userid= Userlogin();
            var list = MessageServices.ListMessages(model, out rowCount, out Name);
            var resultModel = new MessagerPaginationResult()
            {
                ConverSationID=model.ConverSationId,
                Page = model.Page,
                RowCount = rowCount,
                PageSize = model.PageSize,
                Data = list,
                Name = Name
                
            };
            return View(resultModel);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MesssageListLoad(MessageSearchingCondition model)
        {
            long rowCount = 0;
            string Name = "";
            model.userid = Userlogin();
            var list = MessageServices.ListMessages(model, out rowCount, out Name);
            var resultModel = new MessagerPaginationResult()
            {
                ConverSationID = model.ConverSationId,
                Page = model.Page,
                RowCount = rowCount,
                PageSize = model.PageSize,
                Data = list,
                Name = Name

            };
            return View(resultModel);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult GetConverSation(int id)
        {
            if (MessageServices.CheckUserInConverSation(Userlogin(), id))
            {
                ViewData["ConverSationID"] = id;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Message");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Send(int ConverSationID,string Content)
        {
            int count = 0;
            try
            {

                MessageInput messageInput = new MessageInput()
                {               
                    ConverSationID = ConverSationID,
                    Content=Content,
                    userid = null,
                    SenderId = Userlogin()
                };
                count = MessageServices.Send(messageInput);
                MessageHub.SendMessage();
                return CreateSuccessResult("Thành công");
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult UpdateSeen(int id)
        {
            bool result = false;
            try
            {
                result = MessageServices.UpdateSeenMessage(Userlogin(),id);
                return CreateSuccessResult("Thành công");
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất Bại");
            }           
        }
    }
}