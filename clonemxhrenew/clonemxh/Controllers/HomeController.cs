﻿using Bussiness;
using Core;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace clonemxh.Controllers
{
    public class HomeController : _BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Userlogin() == 0)
            {
                return RedirectToAction("Login","User");
            }
            else
            {
                return View();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult HomeSideBar()
        {
            int countmess = 0;
            countmess = MessageServices.CountMessage(Userlogin());
            ViewData["CountMess"] = countmess;
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult NotFound()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult HomeFriendList(UserSearchingCondition model)
        {
            model.Status = StatusFriend.Acept;
            model.UserId = Userlogin();
            long rowCount = 0;
            var list = new List<UserEntity>();
            list = FriendSevice.Friendlish(model, out rowCount).ToList();
            var resultModel = new UserPaginationResult()
            {
                Page = model.Page,
                RowCount = rowCount,
                PageSize = model.PageSize,
                ListFollow = list
            };
            //Session[SESSION_GAMECATEGORY_SEARCHCONDITIONS] = model;
            return View(resultModel);
        }
    }
}