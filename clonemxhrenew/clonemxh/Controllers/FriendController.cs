﻿using Bussiness;
using Core;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace clonemxh.Controllers
{
    [Authorize]
    public class FriendController : _BaseController
    {
        // GET: Friend
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddFriend(int id)
        {           
    
            try
            {
                Friend f = new Friend()
                {
                    UserID2 = id,
                    UserID1 = Userlogin(),
                    Status = StatusFriend.Request
                };
             int res = FriendSevice.AddFriend(f);
                int noti = Notification(res, id, EntityType.AddFriend);
                CallNotification();
                return CreateSuccessResult("Thành công");                
            }
            catch(Exception ex) {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ///
        [HttpPost]
        public ActionResult Acept(int id)
        {
            bool result = false;
            try
            {
                result = FriendSevice.Acept(Userlogin(), id);
                if (result)
                {
                    int friend = FriendSevice.CheckFriend(Userlogin(), id).FriendID;
                    int noti = Notification(friend, id, EntityType.AceptFriend);
                    CallNotification();
                    return CreateSuccessResult("Thành công");
                }
                else
                {
                    return CreateFailResult("Thất Bại");
                }
          
            }
            catch(Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// Hủy kết bạn
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Unfriend(int id)
        {
            bool result = false;
            try
            {
                result = FriendSevice.UnFriend(Userlogin(), id);
                if (result)
                {
                    CallNotification();
                    return CreateSuccessResult("Thành công");
                }
                else
                {
                    return CreateFailResult("Thất Bại");
                }
            }
            catch (Exception ex)
            {
                return CreateFailResult("Thất bại");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FriendList(UserSearchingCondition model)
        {
            model.Status = StatusFriend.Acept;
            long rowCount = 0;
            var list  = new List<UserEntity>();
           list = FriendSevice.Friendlish(model, out rowCount).ToList();
            var resultModel = new UserPaginationResult()
            {
                Page = model.Page,
                RowCount = rowCount,
                PageSize = model.PageSize,
                ListFollow = list
            };
            return View( resultModel);
        }

    }
}