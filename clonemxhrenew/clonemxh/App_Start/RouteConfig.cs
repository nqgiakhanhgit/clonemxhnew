﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace clonemxh
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
            name: "Default",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
            name: "EditProfile",
            url: "User/Edit",
            defaults: new { controller = "User", action = "Edit" }
            );
            routes.MapRoute(
            name: "ListFollower",
            url: "Follow/ListFollower/{Page}/{PageSize}/{UserId}",
            defaults: new { controller = "Follow", action = "ListFollower", Page = "", PageSize = "", UserId = "" }
            );
            routes.MapRoute(
            name: "ListFollow",
            url: "Follow/ListFollow/{Page}/{PageSize}/{UserId}",
            defaults: new { controller = "Follow", action = "ListFollow", Page = "", PageSize = "", UserId = "" }
            );
            routes.MapRoute(
            name: "BlockList",
            url: "Block/BlockList/{Page}/{PageSize}/{UserId}",
            defaults: new { controller = "Block", action = "BlockList", Page = "", PageSize = "", UserId = "" }
            );
            routes.MapRoute(
            name: "FriendList",
            url: "Friend/FriendList/{Page}/{PageSize}/{UserId}",
            defaults: new { controller = "Friend", action = "FriendList", Page = "", PageSize = "", UserId = "" }
            );
                        routes.MapRoute(
            name: "AddPost",
            url: "Post/Add/{id}/{postType}",
            defaults: new { controller = "Post", action = "Add",id="",postType=""}
            );
            routes.MapRoute(
            name: "EditPost",
            url: "Post/Edit/{id}",
            defaults: new { controller = "Post", action = "Edit", id = "" }
            );
            routes.MapRoute(
            name: "DeletePost",
            url: "Post/Delete/{id}",
            defaults: new { controller = "Post", action = "Delete", id = "" }
            );
            routes.MapRoute(
            name: "DeleteComment",
            url: "Comment/Delete/{id}",
            defaults: new { controller = "Comment", action = "Delete", id = "" }
            );
            routes.MapRoute(
            name: "LeftOutGroup",
            url: "UserInGroup/LeftOut/{id}/{userid}/{superadmin}",
            defaults: new { controller = "UserInGroup", action = "LeftOut", id = "",userid="" ,superadmin=""}
            );
            routes.MapRoute(
            name: "KickOutGroup",
            url: "UserInGroup/Kick/{id}/{userid}",
            defaults: new { controller = "UserInGroup", action = "Kick", id = "", userid = "" }
            );
            routes.MapRoute(
            name: "UsersLikePost",
            url: "Like/ListUserLiked/{id}",
            defaults: new { controller = "Like", action = "ListUserLiked", id = "" }
            );
            routes.MapRoute(
            name: "UserInGroupList",
            url: "UserInGroup/ListTable/{id}",
            defaults: new { controller = "UserInGroup", action = "ListTable", id = "" }
            );
            routes.MapRoute(
            name: "EditGroup",
            url: "Group/Edit/{id}",
            defaults: new { controller = "Group", action = "Edit", id = "" }
            );
            routes.MapRoute(
            name: "GetAvatar",
            url: "User/EditAva/{id}",
            defaults: new { controller = "User", action = "EditAva", id = "" }
            );
            routes.MapRoute(
            name: "EditWallPaper",
            url: "User/EditWallPaper/{id}",
            defaults: new { controller = "User", action = "EditWallPaper", id = "" }
            ); 
            routes.MapRoute(
            name: "BlockNotification",
            url: "Block/BlockNotification/{id}",
            defaults: new { controller = "Block", action = "BlockNotification", id = "" }
            );
            routes.MapRoute(
            name: "ViewAllNotification",
            url: "Notification/ViewAll/{Page}/{PageSize}",
            defaults: new { controller = "Notification", action = "ViewAll", Page = "",PageSize="" }
            );
            routes.MapRoute(
            name: "DeleteConverSation",
            url: "DeleteMessage/GetDeleteConvesation/{id}",
            defaults: new { controller = "DeleteMessage", action = "GetDeleteConvesation", id = "" }
            );
            routes.MapRoute(
            name: "DeleteMessageOnly",
            url: "DeleteMessage/GetDeleteMessageOnly/{id}",
            defaults: new { controller = "DeleteMessage", action = "GetDeleteMessageOnly", id = "" }
            );
            routes.MapRoute(
            name: "DeleteMessage",
            url: "DeleteMessage/GetDeleteMessage/{id}",
            defaults: new { controller = "DeleteMessage", action = "GetDeleteMessage", id = "" }
            );
            routes.MapRoute(
            name: "ListPostJson",
            url: "Post/ListPostJson/{userid}/{page}/{pagesize}/{postType}",
            defaults: new { controller = "Post", action = "ListPostJson", userid="" ,page ="",pagesize="",postType="" }
            );

        }
    }
}
