﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clonemxh
{
    public class Notificationhub:Hub
    {
        public static void SendNotification()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<Notificationhub>();
            context.Clients.All.receiveNotification();
        }
    }
}