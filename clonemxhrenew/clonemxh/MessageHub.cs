﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clonemxh
{
    public class MessageHub:Hub
    {
        /// <summary>
        /// 
        /// </summary>
        public static void SendMessage()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<MessageHub>();
            context.Clients.All.receiveMessage();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public static void DeleteMess(int id) {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<MessageHub>();
            context.Clients.All.deleteMess(id);
        }
    }
}