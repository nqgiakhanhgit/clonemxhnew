﻿
var applicationUrl;
//refresh trang
function refreshPage() {
    window.location.reload(true);
}
//chuyển trang
function redirectPage(link) {
    window.parent.location = link;
}

function createBaseLink(url) {
    return  url;
}
//Lấy nội dung của 1 trang có địa chỉ tại link (dùng POST method và truyền dữ liệu ở postData) và hiển thị nội dung trong container
function postPageContent(container, link, postData) {
    var res;
    $.ajax({
        url: link,
        type: "POST",
        data: postData,
        async: false,
        beforeSend: function () {
            showLoader();
        },
        complete: function () {
            hideLoader();
        },
        error: function (error) {
            alert("Your request is not valid!");
        },
        success: function (data) {
            // Data là 1 đoạn html
            res = data;
            $(container).append(data);
        }
    });
    return res;
}
function postPageContentV2(container, link, postData) {
    $.ajax({
        url: link,
        type: "POST",
        data: postData,
        async: true,
        beforeSend: function () {
            showLoader();
        },
        complete: function () {
            hideLoader();
        },
        error: function (error) {
            alert("Your request is not valid!");
        },
        success: function (data) {
            // Data là 1 đoạn html
            $(container).html(data);
        }
    });
}
function postPageContentV3(container, link, postData) {
    $.ajax({
        url: link,
        type: "POST",
        data: postData,
        async: true,
        beforeSend: function () {
            showLoader();
        },
        complete: function () {
            hideLoader();
        },
        error: function (error) {
            alert("Your request is not valid!");
        },
        success: function (data) {
            // Data là 1 đoạn html
            $(container).prepend(data);
        }
    });
}
function getPageContent(container, link, postData) {
    //showLoader();
    $.ajax({
        url: link,
        type: "GET",
        data: postData,
        async: true,
        beforeSend: function () {
            showLoader();
        },
        complete: function () {
            hideLoader();
        },
        error: function (error) {
            alert("Your request is not valid!");
        },
        success: function (data) {
            // Data là 1 đoạn html
            $(container).prepend(data);
        }
    });
}
function getPageContentV2(container, link, postData) {
    //showLoader();
    $.ajax({
        url: link,
        type: "GET",
        data: postData,
        async: true,
        beforeSend: function () {
            showLoader();
        },
        complete: function () {
            hideLoader();
        },
        error: function (error) {
            alert("Your request is not valid!");
        },
        success: function (data) {
            // Data là 1 đoạn html
            $(container).html(data);
        }
    });
}
function getPageContentV3(container, link, postData) {
    //showLoader();
    $.ajax({
        url: link,
        type: "GET",
        data: postData,
        async: true,
        beforeSend: function () {
            showLoader();
        },
        complete: function () {
            hideLoader();
        },
        error: function (error) {
            alert("Your request is not valid!");
        },
        success: function (data) {
            // Data là 1 đoạn html
            $(container).append(data);
        }
    });
}
//bắt các link popup
//function triggerPopupLink() {
//    $(".popup-link").click(function (e) {
//        e.preventDefault();
//        popupLink(this);
//    });
//}
function triggerPopupLink() {
    $('body').on('click', '.popup-link', function (e) {
        e.preventDefault();
        popupLink(this);
    });
}
function showLoader() {
    console.log("Show Load");
    $('#loading').css('display', 'block');
}
function hideLoader() {
    console.log("Hide")
    $('#loading').css('display','none');
}
function popupLink(obj) {
    var container = "#dialogMain";
    var url = $(obj).attr("data-href");
    var w = $(obj).data("width");
    var h = $(obj).data("height");
    $.ajax({
        type: "GET",
        url: url,
        beforeSend: function () {
            showLoader();
        },
        complete: function () {
            hideLoader();
        },
        async: true,
        error: function () {
            alert("Your request is not valid!");
        },
        success: function (data) {
            $(container).html(data);
            $(container).modal();
            $(container + " .modal-dialog").attr({ 'style': 'max-width:' + w + 'px !important;min-height:' + h + 'px' });
        }
    });
}



function postData(url, data) {
    var result;
    $('.btn-submit').attr("disabled", 'disabled');
    console.log("submit")
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        async: false,
        processData: false, // Không xử lý dữ liệu
        contentType: false, // Không đặt tiêu đề Content-Type
        beforeSend: function () {
            showLoader();
        },
        complete: function () {
            hideLoader();
        },
        error: function () {
            result = "Invalid Request!";
        },
        success: function (res) {
            result = res;
        }
    });
    return result;
}

function postDataV1(url, data) {
    var result;
    $('.btn-submit').attr("disabled", 'disabled');
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        async: false,
        beforeSend: function () {
            showLoader();
        },
        complete: function () {
            hideLoader();
        },
        error: function () {
            result = "Invalid Request!";
        },
        success: function (res) {
            result = res;
        }
    });
    return result;
}

