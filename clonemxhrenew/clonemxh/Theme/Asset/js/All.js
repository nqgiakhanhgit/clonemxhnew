﻿//header//
//function toggleSidebars() {
//    var windowWidth = $(window).width();
//    if (windowWidth < 992) { // Kích thước tùy chỉnh của Bootstrap để co lại
//        $('.itemhide').show();
//        $("#sidebar").hide();
//    } else {
//        $("#sidebar").show();
//        $('.itemhide').hide();
//    }
//}

////
function toggleSidebar() {
    $('#sidebar').toggleClass('show-sidebar');
}

var pagemess = 1;
//COmment
    function AddComment(data, PostID, ParentID) {
        if (data != null) {
            var result = postData('/Comment/Add', data);
            if (result.Code === 1) {
                // Lấy giá trị postID
                $('input[data-id="' + PostID + '"]').val('');
                // Thêm comment mới
                if (ParentID === 0) {
                    getPageContentV3("#comment" + PostID, "/Comment/Get/" + result.Data);
                }
                else {
                    getPageContentV3(".repcomment" + ParentID, "/Comment/Get/" + result.Data);
                }

                // Load lại số lượng bình luận bằng cách get post
                getPageContentV2("#content-post" + PostID, "/Post/Get/" + PostID);
            }
            else {
                console.log(result.Msg);
            }
        }

    }
    // Nhấn "Bình luận" để bình luận
    $('body').on('click', '#btnComment', function () {
        var PostID = $(this).data('id');
        var Content = $(this).closest('.input-group').find('input[data-id="' + PostID + '"]').val();
        var ParentID = $(this).data('parentid');
        if (Content.trim() === '') {
            return;
        }
        var data = new FormData();
        data.append('PostID', PostID);
        data.append('Content', Content);
        data.append('ParentID', ParentID);
        AddComment(data, PostID, ParentID);
    });
    // Nhấn enter để bình luận
$('body').on('keypress', '#inputcomment', function (event) {
    if (event.which === 13) {
        var PostID = $(this).data('id');
        var Content = $(this).val();
        var ParentID = $(this).data('parentid');
        if (Content.trim() === '') {
            return;
        }
        var data = new FormData();
        data.append('PostID', PostID);
        data.append('Content', Content);
        data.append('ParentID', ParentID);
            AddComment(data, PostID, ParentID);
        }
    });
    
    function CommentLoad(data) {
        var postUrl = createBaseLink("/Comment/ListComment?Page=" + data["Page"]);
        // Append bình luận mới vào bài viết có ID là PostID
        postPageContentV2("#comment" + data["PostID"], postUrl, data);
    }
    // Tải toàn bộ bình luận của bài viết
    function AllComment(PostID) {
        var data = {
            PostID: PostID,
            Page: 0,
            PageSize: 0,
            ParentID: 0
        }
        CommentLoad(data);
    }
    // Xóa bình luận theo id
    function DeleteComment(id, postid) {
        if (id == 0 || id == null) {
            return;
        }
        var data = new FormData();
        data.append('id', id);
        var result = postData('/Comment/DeleteComment/', data);
        let html = '';
        if (result.Code === 1) {
            html += ` <div class="alert alert-success alert-dismissible">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             <strong>${result.Msg} </strong>
         </div>`;
        }
        else {
            html += `<div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>${result.Msg}</strong>
        </div>`
        }
        $('#notimodal').html(html);
        getPageContentV2("#content-post" + postid, "/Post/Get/" + postid);
        $('#comment-block' + id).closest('div').remove();
        setTimeout(function () {
            // Ẩn modal có id là "dialogMain" sau khi đã đợi 2-3 giây (2000-3000ms)
            $('#dialogMain').modal('hide');
        }, 2000);
    }
    // tải giao diện chỉnh sửa bình luận
    function EditComment(id) {
        let data = {
            id: id
        }
        getPageContentV2('.comment-content' + id, '/Comment/Edit/' + id, data);
    }
    //Chỉnh sửa bình luận (Xác nhận chỉnh sửa)
function UpdateComment(id) {
    var Content = $('#inputeditcomment' + id).val();
    if (Content.trim() === '') {
        return;
    }
        let data = new FormData()
        data.append("CommentID", id);

        data.append("Content", Content );
        let result = postData('/Comment/Update', data);
        if (result.Code === 1) {
            // tải HTML
            getPageContentV2('.comment-content' + id, '/Comment/GetContent/' + id, id);
        }
        else {
            // tải HTML
            getPageContentV2('.comment-content' + id, '/Comment/GetContent/' + id, id);
            alert(result.Msg);
        }
    }
    // Hủy chỉnh sửa bình luận
    function CancelComment(id) {
        if (id === 0 || id === null) {
            return;
            refreshPage();
        }
        else {
            // tải HTML
            getPageContentV2('.comment-content' + id, '/Comment/GetContent/' + id, id);
        }
    }
    //Bấm vào số lượng bình luận hiển thị 3 bình luận đầu tiên và focus vào ô nhập bình luận
    function LoadComment(PostID, ParentID) {
        var data = {
            PostID: PostID,
            Page: 1,
            PageSize: 3,
            ParentID: ParentID
        }

        CommentLoad(data);
        $('.focus' + PostID).focus();
    }
    function RepComment(PostID, CommentID, PartenID) {

        let html = '';
        if (PartenID === 0) {
            html += ` <div class="input-rep input-group input-group-sm mb-0"> <input class="form-control form-control-sm" id="inputrepcomment" data-id="${PostID}" data-parentid="${CommentID}" type="text" autofocus placeholder="Trả lời bình luận">
                      <div class="input-group-append">
                          <button type="button" id="btnRepComment" data-id="${PostID}" data-parentid="${CommentID}" class="btn btn-outline-primary"><i class="fa-solid fa-paper-plane"></i></button>
                      </div></div>`;
            $('.repcommentform' + CommentID).html(html);
        }
        else {
            html += `<div class="input-rep input-group input-group-sm mb-0"> <input class="form-control form-control-sm" id="inputrepcomment" data-id="${PostID}" data-parentid="${PartenID}" type="text" autofocus placeholder="Trả lời bình luận">
                      <div class="input-group-append">
                          <button type="button" id="btnRepComment" data-id="${PostID}" data-parentid="${PartenID}" class="btn btn-outline-primary"><i class="fa-solid fa-paper-plane"></i></button>
                      </div></div>`;
            $('.repcommentform' + PartenID).html(html);
        }


    }
    $('body').on('click', '#btnRepComment', function () {
        var PostID = $(this).data('id');
        var Content = $(this).closest('.input-rep').find('input[data-id="' + PostID + '"]').val();
        var ParentID = $(this).data('parentid');
        if (Content.trim() === '') {
            return;
        }
        var data = new FormData();
        data.append('PostID', PostID);
        data.append('Content', Content);

        data.append('ParentID', ParentID);
        AddComment(data, PostID, ParentID);
    });


$('body').on('keypress', '#inputrepcomment', function (event) {

    if (event.which === 13) {
        var PostID = $(this).data('id');
        var Content = $(this).val();

        if (Content.trim() == '') {
            return;
        }
        var ParentID = $(this).data('parentid');
        var data = new FormData();
        data.append('PostID', PostID);
        data.append('Content', Content);
        data.append('ParentID', ParentID);


            AddComment(data, PostID, ParentID);
        }
    });
    var pagecomment = {};
    function LoadRepComment(PostID, CommentID) {
        if (!pagecomment[CommentID]) {
            pagecomment[CommentID] = 1; // Nếu chưa có page cho id này, set page là 1
        }
        var data = {
            Page: pagecomment[CommentID],
            PageSize: 3,
            PostID: PostID,
            ParentID: CommentID,
        }
        var postUrl = createBaseLink("/Comment/ListComment?Page=" + data["Page"]);
        var res = postPageContent(".repcomment" + CommentID, postUrl, data);
        //console.log(typeof res);
        // trả ra là chuỗi chưa chuẩn hóa
        if (res.trim() === "" || res.trim() === null || res.trim() === '' || res.trim() === ' ') {
            $('.loadrep' + CommentID).hide();
            console.log("hide");

        }
        else {
            pagecomment[CommentID]++;
        }

    }
    // End Comment

    //DeleteMess
    //Xóa tin nhắn vĩnh viễn
    function DeleteMessage(id, userid) {
        let data = {
            id: id,
        }
        let result = postDataV1("/DeleteMessage/DeleteMessage/", data);
        let html = '';
        if (result.Code === 1) {
            html += ` <div class="alert alert-success alert-dismissible">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             <strong>${result.Msg} </strong>
         </div>`;
        }
        else {
            html += `<div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>${result.Msg}</strong>
        </div>`
        }
        $('#notimodal').html(html);
        setTimeout(function () {
            $('#dialogMain').modal('hide');
        }, 2000);

    }
    // Xóa tin nhắn ở phía bạn 
    function DeleteMessageOnly(id) {
        let data = {
            id: id,
        }
        let result = postDataV1("/DeleteMessage/DeleteMessageOnly/", data);
        let html = '';
        if (result.Code === 1) {
            html += ` <div class="alert alert-success alert-dismissible">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             <strong>${result.Msg} </strong>
         </div>`;
            $(`.messre${id}`).closest('div').remove();
            $(`#btn-mess${id}`).closest('div').remove();
        }
        else {
            html += `<div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>${result.Msg}</strong>
        </div>`
   
        }
        $('#notimodal').html(html);
        setTimeout(function () {
            $('#dialogMain').modal('hide');
        }, 2000);

    }
    // Xóa (ẩn cuộc trò chuyện)
    function DeleteConverSation(id) {
        let data = {
            id: id
        }
        let result = postDataV1("/DeleteMessage/DeleteConveSation/", data);
        if (result.Code === 1) {
            window.location.href = "/Message/Index";
        }
    }
    /// End DeleteMess


    //GroupJS
    var pagegroup = 2;
    function ViewMoreGroup(userid) {
        let data = {
            Page: pagegroup,
            PageSize: 3,
            UserId: userid
        };
        pagegroup++;
        let reuslt = postPageContent("#groupjoinedlist", "/Group/GroupJoinedList", data)
        if (reuslt.trim() === '') {
            $('#viewmoregroup').closest('li').remove();

        }
        console.log(reuslt);
    }
    function ViewAllGroup(userid) {
        let data = {
            Page: 0,
            UserId: userid
        };
        let reuslt = postPageContentV2("#groupjoinedlist", "/Group/GroupJoinedList", data)
        $('#viewmoregroup').closest('li').remove();
        $('#viewallgroup').closest('li').remove();
        console.log(reuslt);
    }
    //EndGroup

    // thực hiện việc thích hoặc bỏ thích bài viết
    function Like(id, action) {
        var url = "";
        var replacebtntext = "";
        var actionreplace = "";
        var data = new FormData();
        data.append("PostID", id);
        switch (action) {
            case "like":
                url = "/Like/Add";
                replacebtntext = "Bỏ thích";
                actionreplace = "unlike";
                break;
            case "unlike":
                url = "/Like/UnLike";
                replacebtntext = "Thích";
                actionreplace = "like";
                break;
            default:
                alert("Action is not valid");
        }
        var result = postData(url, data);
        if (result.Code === 1) {
            // tải lại phần nội dung của bài viết
            getPageContentV2("#content-post" + id, '/Post/Get/' + id, id);
        }
        else {
            alert(result.Msg);
        }

    }
    ///////Notification
    let pageNotification;
    let StaticPageSize = 4;
    function notificationLoad(data) {
        var postUrl = createBaseLink("/Notification/List?Page=" + pageNotification);
        postPageContent("#notification", postUrl, data);
        getPageContentV2("#noticount", "/Notification/Count");
    }
    // cuộn để tải thêm bài viết
    function LayzyloadNotification(data) {
        notificationLoad(data);
        UpdateNoti(data["Page"]);
        $(window)
        $('.notification-container').scroll(function () {
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight && data["Page"] < $('#notiPageCount').val()) {
                data["Page"] = data["Page"] + 1;
                UpdateNoti(data["Page"]);
                notificationLoad(data);
            }
        });
    }
    //Push noti
    $(document).ready(function () {
        // ... your existing code

        // Tạo kết nối SignalR
        var notificationhub = $.connection.notificationhub;

        // Lắng nghe thông báo từ server
        notificationhub.client.receiveNotification = function () {
            // Xử lý thông báo
            let datanoti = {
                Page: 1,
                PageSize: StaticPageSize
            }
            let notifiUrl = createBaseLink("/Notification/List?Page=" + datanoti["Page"]);
            postPageContentV2("#notification", notifiUrl, datanoti);
            getPageContentV2("#noticount", "/Notification/Count");
        };
        // Khởi động kết nối SignalR
        $.connection.hub.start().done(function () {
        });
    });
    ///
    function UpdateNoti(Page) {
        let PageSize = StaticPageSize;
        let dataupdate = new FormData();
        dataupdate.append('Page', Page);
        dataupdate.append('PageSize', PageSize);
        let kq = postData('/Notification/Update', dataupdate);
        if (kq.Code === 1) {
            getPageContentV2("#noticount", "/Notification/Count");
        }
        else {
            console.log("lỗi");
        }
    }
    /// Cập nhật thông báo được chọn thành đã đọc
    // Có thể click vào biểu tượng hoặc click vào link thông báo.
    function UpdateToRead(notiid) {
        let kq;
        let dataupdate = new FormData();
        dataupdate.append('id', notiid);
        kq = postData('/Notification/UpdateToReaded', dataupdate);
        if (kq.Code === 1) {
            let iddat = {
                id: notiid
            }
            getPageContentV2(`.notiItem${notiid}`, "/Notification/Get/", iddat);
        }
        return kq;
    }
    // Cập nhật tất cả thông báo thành đã đọc
    function Update_All_Notification() {
        let kq;
        kq = postData('/Notification/UpdateAll');
        if (kq.Code === 1) {

            let datanoti = {
                Page: 1,
                PageSize: StaticPageSize
            }
            let notifiUrl = createBaseLink("/Notification/List?Page=" + datanoti["Page"]);
            postPageContentV2("#notification", notifiUrl, datanoti);
        }
        else {
            console.log("Lỗi");
        }

    }
    ///
    ////////////////////Post////////////////////////////////////////////
    // Khai báo trang
    var page;
    // Sử dụng với lazyload
    function postLoad(data) {
        var postUrl = createBaseLink("/Post/ListPost?Page=" + data["Page"]);
        postPageContent("#post", postUrl, data);
    }
    // cuộn để tải thêm bài viết
    function Layzyload(data) {
        postLoad(data);
        $(window)
            .scroll(function () {
                if ($(window).scrollTop() >= $(document).height() - $(window).height()-500 && data["Page"] < $('#PageCount').val()) {
                    data["Page"] += 1;
                    postLoad(data);
                }
            });
    }
    // Xóa bài viết theo postID

    /////////////////////////////////////////////////////////////////////////
    /////////////////////RelationShip///////////////////////////////////////////

    function manageRelationShip(userid, action) {
        let data = new FormData();
        data.append('id', userid);
        let url = "";
        let replaceBtnText = "";
        let actionreplace = '';
        let classrplace = '';
        console.log(actionreplace);
        switch (action) {
            case "add":
                url = "/Friend/AddFriend";
                replaceBtnText = "Hủy";
                actionreplace = "unfriend";
                classrplace = 'repfrbtn' + userid;
                break;
            case "accept":
                url = "/Friend/Acept";
                replaceBtnText = "Hủy kết bạn";
                actionreplace = "unfriend";
                classrplace = 'repfrbtn' + userid;
                break;
            case "unfriend":
                url = "/Friend/UnFriend";
                replaceBtnText = "Thêm Bạn Bè";
                actionreplace = "add";
                classrplace = 'repfrbtn' + userid;
                break;
            case "addfollow":
                url = "/Follow/AddFollow";
                replaceBtnText = "Bỏ theo dõi";
                actionreplace = "unfollow";
                classrplace = 'flmanager' + userid;
                break;
            case "unfollow":
                url = "/Follow/UnFollow";
                replaceBtnText = "Theo dõi";
                actionreplace = "addfollow";
                classrplace = 'flmanager' + userid;
                $('.flrow' + userid).closest('tr').remove();
                break;
            case "block":
                url = "/Block/AddBlock";
                window.location.reload();
                break;
            case "unblock":
                url = "/Block/UnBlock";
                window.location.reload();
                break;
            default:
                alert("Invalid action");
                return;
        }
        var result = postData(url, data);
        if (result.Code === 1) {
            refreshPage();
        } else {
            alert("MakePopup");
        }
    }
    ///////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////UserInGroup/////////////////////////////////////////////////////////
                function GroupAction(userid, id,status, action) {
        let data = new FormData();
        data.append("GroupID", id);
        data.append("userid", userid);
 
        let url = "";
        switch (action) {
            case "Add":
                url = "/UserInGroup/Add";
                data.append("Status", status);
                break;
            case "Delete":
                url = "/UserInGroup/OutGroup";
                break;
            default:
                alert("Action Is not valid");
        }
        var result = postData(url, data);
        if (result.Code === 1) {
            window.location.reload();
            } else {
                console.log(result);
            }
    }
    //Cập nhật Vai trò, chấp nhận vào nhóm
    function UpdateUserInGroup(id,roleid, userid, groupid, status) {
        let data = {
            UserId: userid,
            UserInGroupId: id,
            GroupID: groupid,
            Status: status,
            RoleID: roleid
        }
        var result = postDataV1('/UserInGroup/Update', data);
        console.log(result);
        if (result.Code === 1) {
            if ($('#calllist')) {
                CallList(0, groupid);
            }
            else
            {
                console.log(result);
            }
        }
    }

    function UpdateSuperAdmin( userid, groupid) {
        let data = new FormData()
        data.append("userid", userid);
        data.append("groupid", groupid);
        var result = postData('/UserInGroup/UpdateSuperAdmin', data);
        if (result.Code === 1) {
            if ($('#calllist')) {
                CallList(0,groupid);
            }
            else {
                console.log(result);
            }
        }
    }


    function CallList(status,groupid) {
        let datalist = {
            Page: 1,
            PageSize: 10,
            Status: status,
            GroupID: groupid,
        }
        postPageContentV2('#calllist', '/UserInGroup/ListUserInGroup/', datalist);
    }

    ///////////////////////////////////////Mess/....................
    $(document).ready(function () {
        // ... your existing code

        // Tạo kết nối SignalR
        var messageHub = $.connection.messageHub;

        // Lắng nghe thông báo từ server
        messageHub.client.receiveMessage = function () {
            let ConverSationId = $("#currentConverSation").val();

            let url = window.location.href;
            if (hasMessageString(url)) {
                Message(ConverSationId, 1, 20);
                postPageContentV2("#sidebar", '/Message/MessSideBar');
            }
            if (isHomePage(url)) {
                postPageContentV2("#sidebar", '/Home/HomeSideBar');
            }
        };
        messageHub.client.deleteMess = function (id) {
            $(`#content${id}`).text("Tin nhắn đã xóa");
            $(`#btn-mess${id}`).closest('div').remove();
        }
        // Khởi động kết nối SignalR
        $.connection.hub.start().done(function () {
        });
    });
    function Message(id, page, pagesize) {
        let datamesage = {
            Page: page,
            PageSize: pagesize,
            ConverSationId: id
        }
        postPageContentV2(`#message${id}`, '/Message/MesssageList', datamesage);

    }
    function SendMessage(data, ConverSationId) {
        if (data != null) {
            var result = postData('/Message/Send', data);
            if (result.Code === 1) {
                Message(ConverSationId, 1, 20);
                $('#inputmess').val('');
            }
            else {
                console.log(result);
            }
        }
    }
    // Nhấn "Bình luận" để bình luận
    $('body').on('click', '#btnsend', function () {
        let ConverSationId = $(this).data('id');
        let Content = $(this).closest('.input-group').find('input[data-id="' + ConverSationId + '"]').val();
        if (Content.trim() === '') {
            return;
        }
        let data = new FormData();
        data.append('ConverSationID', ConverSationId);
        data.append('Content', Content);
        SendMessage(data, ConverSationId);
    });
    // Nhấn enter để bình luận
$('body').on('keypress', '#inputmess', function (event) {
    if (event.which === 13) {
        let ConverSationId = $(this).data('id');
        let Content = $(this).val();
        // Kiểm tra nếu nội dung trống
        if (Content.trim() === '') {
            return; // Ngăn người dùng đăng bài nếu nội dung rỗng
        }

            let data = new FormData();
            data.append('ConverSationID', ConverSationId);
            data.append('Content', Content);
            SendMessage(data, ConverSationId);
        }
    });
    function UpdateSeen(id) {
        let dataupdate = new FormData();
        dataupdate.append("id", id);
        let result = postData("/Message/UpdateSeen", dataupdate);
        if (result.Code === 1) {
            postPageContentV2("#sidebar", '/Message/MessSideBar');
        }
        else {
            console.log(result);
        }
    }
    function hasMessageString(url) {
        return url.toLowerCase().includes("message");
    }


