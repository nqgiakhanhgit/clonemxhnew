﻿using Bussiness;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


namespace clonemxh
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            
            AreaRegistration.RegisterAllAreas();
            
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            RouteConfig.RegisterRoutes(RouteTable.Routes);

            BundleConfig.RegisterBundles(BundleTable.Bundles);

            string connectionString = ConfigurationManager.ConnectionStrings["CloneMXH"].ConnectionString;

                UserService.Init(DatabaseType.SqlServer, connectionString);

                FriendSevice.Init(DatabaseType.SqlServer, connectionString);

                PostSevice.Init(DatabaseType.SqlServer, connectionString);

                GroupService.Init(DatabaseType.SqlServer, connectionString);

                SearchService.Init(DatabaseType.SqlServer, connectionString);

                NotificationService.Init(DatabaseType.SqlServer, connectionString);

                MessageServices.Init(DatabaseType.SqlServer, connectionString);


            //   GlobalHost.Configuration.DefaultMessageBufferSize = 100;


        }
    }
}
