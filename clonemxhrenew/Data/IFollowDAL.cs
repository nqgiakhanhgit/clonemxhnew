﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface IFollowDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fl"></param>
        /// <returns></returns>
        int AddFollow(Follow fl);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        bool UnFollow(int userid1, int userid2);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        Follow CheckFollow(int userid1, int userid2);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        IList<UserEntity> FollowList(UserSearchingCondition model,out long rowcount);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        IList<UserEntity> FollowerList(UserSearchingCondition model, out long rowcount);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int FollowCount(int id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int FollowerCount(int id);
    }
}
