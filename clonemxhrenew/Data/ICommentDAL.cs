﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface ICommentDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmt"></param>
        /// <returns></returns>
        int Add(Comment cmt);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Comment Get(int id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(int id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Count(int id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        IList<Comment> ListComment(CommentSearchingCondition model, out long rowCount,out int RoleID);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        bool Update(Comment comment);
    }
}
