﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Data
{
    public interface ISearchDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        IList<Search> Result(SearchCondition model, out long rowCount);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        IList<PostEx> ListPost(PostSearchingCondition model, out long rowcount);
    }
}
