﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface IUserInGroupDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="useringroup"></param>
        /// <returns></returns>
        int Add(UserInGroup useringroup);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="useringroup"></param>
        /// <returns></returns>
        bool Update(UserInGroup useringroup);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="useringroup"></param>
        /// <returns></returns>
        bool Delete(int userid,int groupid);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        UserInGroup Get(int userid,int groupid);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupid"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IList<UserInGroup>UserInGroupList(UserInGroupSearchingCondition model,out long rowCount);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="groupid"></param>
        /// <returns></returns>
        bool UpdateSuperAdmin(int userid, int groupid);
    }
}
