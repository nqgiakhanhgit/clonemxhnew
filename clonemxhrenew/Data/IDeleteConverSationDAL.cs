﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface IDeleteConverSationDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conversationid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        int Add(int conversationid, int userid);
    }
}
