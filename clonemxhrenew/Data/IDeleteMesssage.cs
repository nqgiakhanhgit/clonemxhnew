﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface IDeleteMesssage
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conversationid"></param>
        /// <param name="messageid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        int Add(int messageid, int userid);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageid"></param>
        /// <returns></returns>
        bool DeleteMessage(int messageid);
    }
}
