﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface  IFriendDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="friend"></param>
        /// <returns></returns>
        int AddFriend(Friend friend);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        bool Acept(int userid1,int userid2);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        bool UnFriend(int userid1,int userid2);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        Friend GetFriend(int userid1,int userid2);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<UserEntity> FriendList(UserSearchingCondition mode, out long rowcount);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Count(int id);
    }
}
