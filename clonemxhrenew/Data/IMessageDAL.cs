﻿using Core.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Data
{
    public interface IMessageDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        int Send(MessageInput data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(int id);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<ConverSationResult> GetConversations(int userid);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        IList<Messages> ListMessage(MessageSearchingCondition model, out long rowCount,out string Name);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Messages Get(int id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        int Count(int userid);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        bool UpdateSeen(int userid,int convesationid);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="converid"></param>
        /// <returns></returns>
        int CheckUserInConverSation(int userid,int conversationid);
    }
}
