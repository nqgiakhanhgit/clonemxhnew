﻿using Core;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class DeleteMessageDAL : _BaseDAL, IDeleteMesssage
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConectionStrings"></param>
        public DeleteMessageDAL(string ConectionStrings) : base(ConectionStrings) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conversationid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public int Add(int messageid, int userid)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_hiden_message_with_messageid", connection);
                cmd.Parameters.Add(CreateParameter("@messageid", messageid));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", userid));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageid"></param>
        /// <returns></returns>
        public bool DeleteMessage(int messageid)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_delete_message", connection);          
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@messageid", messageid));
                result = cmd.ExecuteNonQuery()>0;
                connection.Close();
            }
            return result;
        }
    }
}
