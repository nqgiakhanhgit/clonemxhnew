﻿using Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class GroupDAL:_BaseDAL,IGroupDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConnectionString"></param>
        public GroupDAL(string ConnectionString):base(ConnectionString) { }
        public int Add(GroupMXH group)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_add_group", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@groupname",group.GroupName));
                cmd.Parameters.Add(CreateParameter("@userid", group.SuperAdmin));
                cmd.Parameters.Add(CreateParameter("@image", group.GrouPicture));
                cmd.Parameters.Add(CreateParameter("@privacymode", group.PrivacyMode));
                count =Convert.ToInt32(cmd.ExecuteScalar());
            }
            return count;
        }

        public GroupMXH Get(int id)
        {
            GroupMXH group  = new GroupMXH();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_get_group", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@groupid", id));
                using (var dbreader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbreader.Read())
                    {
                        group = new GroupMXH()
                        {
                            GroupID = Convert.ToInt32(dbreader["GroupID"]),
                            GroupName = Convert.ToString(dbreader["GroupName"]),
                            GrouPicture = Convert.ToString(dbreader["GroupPicture"]),
                            PrivacyMode = Convert.ToInt32(dbreader["PrivacyMode"]),
                            SuperAdmin = Convert.ToInt32(dbreader["SuperAdmin"]),
                            UserFirstName = Convert.ToString(dbreader["userFirstName"]),
                            UserLastName = Convert.ToString(dbreader["userLastName"]),
                            Avartar= Convert.ToString(dbreader["avatar"])
                        };
                    }
                    dbreader.Close();
                }
                   
            }
            return group;
        }

        public bool Update(GroupMXH group)
        {
           bool result  = false;
            using(SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("[proc_update_group]", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@groupid", group.GroupID));
                cmd.Parameters.Add(CreateParameter("@groupname", group.GroupName));
                cmd.Parameters.Add(CreateParameter("@image", group.GrouPicture)); 
                cmd.Parameters.Add(CreateParameter("@privacymode", group.PrivacyMode));
                result  = cmd.ExecuteNonQuery()>0;
            }
            return result;
        }

        public bool Delete(int id)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("[proc_delete_group]", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@groupid", id));
                result = cmd.ExecuteNonQuery() > 0;
            }
            return result;
        }

        public IList<GroupMXH> GroupJoinList(GroupSearchingCondition model,out long rowCount)
        {
            List<GroupMXH> data = new List<GroupMXH>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_group_joined_list", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@Page", model.Page));
                cmd.Parameters.Add(CreateParameter("@PageSize",model.PageSize));
                cmd.Parameters.Add(CreateParameter("@userid", model.UserId));
                cmd.Parameters.Add(CreateOutputParameter("@rowCount", SqlDbType.BigInt));
                using (var dbreader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbreader.Read())
                    {
                        data.Add( new GroupEx()
                        {
                            GroupID = Convert.ToInt32(dbreader["GroupID"]),
                            GroupName = Convert.ToString(dbreader["GroupName"]),
                            GrouPicture = Convert.ToString(dbreader["GroupPicture"]),
                        });
                    }
              
                    dbreader.Close();
                }
                rowCount = Convert.ToInt64(cmd.Parameters["@rowCount"].Value);

            }
            return data;
        }
    }
}
