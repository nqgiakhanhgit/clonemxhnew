﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class ConverSationDAL:_BaseDAL,IConverSationDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionstring"></param>
        public ConverSationDAL(string connectionstring): base(connectionstring) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="conversationid"></param>
        /// <returns></returns>
        public int Add(int creator, string userid, out int conversationid)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("[proc_create_new_conversation]", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@creator", creator));
                cmd.Parameters.Add(CreateParameter("@userid", userid));
                cmd.Parameters.Add(CreateOutputParameter("@conversation", 0));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
                conversationid = Convert.ToInt32(cmd.Parameters["@conversation"].Value);
            }
            return count;
        }
    }
}
