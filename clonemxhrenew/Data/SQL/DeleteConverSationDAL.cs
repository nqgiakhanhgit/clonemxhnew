﻿using Core;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class DeleteConverSationDAL : _BaseDAL, IDeleteConverSationDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConectionStrings"></param>
        public DeleteConverSationDAL(string ConectionStrings) : base(ConectionStrings) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conversationid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public int Add(int conversationid, int userid)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_hiden_conversation", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", userid));
                cmd.Parameters.Add(CreateParameter("@conversationid", conversationid));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
    }
}

