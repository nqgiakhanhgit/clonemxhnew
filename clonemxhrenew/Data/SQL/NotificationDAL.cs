﻿using Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class NotificationDAL : _BaseDAL, INotificationDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionstring"></param>
        public NotificationDAL(string connectionstring) : base(connectionstring) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Add(NotificationInsert model)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                switch (model.Entity_Type_Id)
                {
                    case 1:
                        cmd = CreateCommand("add_notification_add_post_in_group", connection);
                        break;
                    case 2:
                        cmd = CreateCommand("add_notification_comment_on_post", connection);
                        break;
                    case 6:
                        cmd = CreateCommand("add_notification_join_group", connection);
                        break;
                    default:
                        cmd = CreateCommand("proc_notification", connection);
                        break;

                }
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@entity_type_id", model.Entity_Type_Id));
                cmd.Parameters.Add(CreateParameter("@entity_id", model.Entity_Id));
                cmd.Parameters.Add(CreateParameter("@notifier_id", model.Notitfier_Id));
                cmd.Parameters.Add(CreateParameter("@actor_id", model.Actor_Id));
                cmd.Parameters.Add(CreateParameter("@status", model.Status));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int Count(int id)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd  = CreateCommand("proc_count_notification", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@notifier_Id", id));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public NotificationResult Get(int id)
        {
            NotificationResult data = new NotificationResult();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("proc_get_notification", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                using (var dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data=new NotificationResult()
                        {
                            Notification_Id = Convert.ToInt32(dbReader["Notification_Id"]),

                            Actor_Id = Convert.ToInt32(dbReader["Actor_Id"]),

                            Entity_Type_Id = Convert.ToInt32(dbReader["Entity_Type_Id"]),

                            userFirstName = Convert.ToString(dbReader["userFirstName"]),

                            userLastName = Convert.ToString(dbReader["userLastName"]),

                            EntityId = Convert.ToInt32(dbReader["EntityId"]),

                            Description = Convert.ToString(dbReader["Description"]),

                            Avatar = Convert.ToString(dbReader["avatar"]),

                            GroupName = dbReader["GroupName"] != DBNull.Value ? Convert.ToString(dbReader["GroupName"]) : "",

                            CommentOnPost = dbReader["CommentOnPost"] != DBNull.Value ? Convert.ToInt32(dbReader["CommentOnPost"]) : 0,

                            Content = dbReader["Content"] != DBNull.Value ? Convert.ToString(dbReader["Content"]) : "",

                            PostInGroup = dbReader["PostInGroup"] != DBNull.Value ? Convert.ToInt32(dbReader["PostInGroup"]) : 0,

                            LikePost = dbReader["LikePost"] != DBNull.Value ? Convert.ToInt32(dbReader["LikePost"]) : 0,

                            Status = Convert.ToInt32(dbReader["Status"]),

                            GroupJoin = dbReader["GroupJoin"] != DBNull.Value ? Convert.ToInt32(dbReader["GroupJoin"]) : 0,

                            TimeSpan = Time(Convert.ToDateTime(dbReader["Create_At"])),

                            ContentPostLiked = dbReader["ContentPostLiked"] != DBNull.Value ? Convert.ToString(dbReader["ContentPostLiked"]) : ""
                        };
                    }
                    dbReader.Close();
                }
                connection.Close();
            }
            return data; ;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        public IList<NotificationResult> ListNotificationResult(NotificationSearchingCondition model, out long rowCount)
        {
            List<NotificationResult> data = new List<NotificationResult>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("proc_notification_list", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@notifier_id", model.Notifier_id);
                cmd.Parameters.AddWithValue("@page", model.Page);
                cmd.Parameters.AddWithValue("@pagesize", model.PageSize);
                cmd.Parameters.Add(CreateOutputParameter("@rowcount", SqlDbType.BigInt));
                using (var dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new NotificationResult()
                        {
                            Notification_Id = Convert.ToInt32(dbReader["Notification_Id"]),

                            Actor_Id = Convert.ToInt32(dbReader["Actor_Id"]),

                            Entity_Type_Id = Convert.ToInt32(dbReader["Entity_Type_Id"]),

                            userFirstName = Convert.ToString(dbReader["userFirstName"]),

                            userLastName = Convert.ToString(dbReader["userLastName"]),

                            EntityId = Convert.ToInt32(dbReader["EntityId"]),

                            Description = Convert.ToString(dbReader["Description"]),

                            Avatar = Convert.ToString(dbReader["avatar"]),

                            GroupName = dbReader["GroupName"] != DBNull.Value ? Convert.ToString(dbReader["GroupName"]) : "",

                            CommentOnPost = dbReader["CommentOnPost"] != DBNull.Value ? Convert.ToInt32(dbReader["CommentOnPost"]) : 0,

                            Content = dbReader["Content"] != DBNull.Value ? Convert.ToString(dbReader["Content"]) : "",

                            PostInGroup = dbReader["PostInGroup"] != DBNull.Value ? Convert.ToInt32(dbReader["PostInGroup"]) : 0,

                            LikePost = dbReader["LikePost"] != DBNull.Value ? Convert.ToInt32(dbReader["LikePost"]) : 0,

                            Status = Convert.ToInt32(dbReader["Status"]),

                            GroupJoin = dbReader["GroupJoin"] != DBNull.Value ? Convert.ToInt32(dbReader["GroupJoin"]) : 0,

                            TimeSpan = Time(Convert.ToDateTime(dbReader["Create_At"])),

                            ContentPostLiked = dbReader["ContentPostLiked"]!=DBNull.Value ? Convert.ToString(dbReader["ContentPostLiked"]):""
                        });
                    }
                    dbReader.Close();
                }
                connection.Close();
                rowCount = Convert.ToInt64(cmd.Parameters["@rowcount"].Value);
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationid"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool Update(int notificationid, int status)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("[proc_update_notification]", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@notification_id", notificationid));
                cmd.Parameters.Add(CreateParameter("@status", status));
                result = cmd.ExecuteNonQuery() > 0;
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notifier_id"></param>
        /// <returns></returns>
        public bool Update_All_Noti(int notifier_id)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("proc_set_all_noti_readed", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@notifierid", notifier_id));               
                result = cmd.ExecuteNonQuery() > 0;
            }
            return result;
        }
    }
}
