﻿using Core;
using Core.Message;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class MessageDAL : _BaseDAL, IMessageDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConnectionString"></param>
        public MessageDAL(string ConnectionString):base(ConnectionString) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="conversationid"></param>
        /// <returns></returns>
        public int CheckUserInConverSation(int userid, int conversationid)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_check_user_in_conversation", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", userid));
                cmd.Parameters.Add(CreateParameter("@conversationid", conversationid));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        } 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public int Count(int userid)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_count_message_seen_yet", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", userid));              
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Messages Get(int id)
        {
            Messages data = new Messages();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_get_mesage", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                using (var dbreader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    if (dbreader.Read())
                    {
                        data = new Messages()
                        {
                            SenderId = Convert.ToInt32(dbreader["SenderId"]),
                            UserLastName = Convert.ToString(dbreader["userLastName"]),
                            Avartar = Convert.ToString(dbreader["avatar"]),
                            Content = Convert.ToString(dbreader["Content"]),
                            Id = Convert.ToInt32(dbreader["Id"]),
                            TimeSpan = Time(Convert.ToDateTime(dbreader["CreateAt"])),
                        };
                    }
                    dbreader.Close();
                }
                connection.Close();
            }
            return data;
        }

        /// <summary>
        /// Tin nhắn cuối cùng của các cuộc trò chuyện
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IList<ConverSationResult> GetConversations(int userid)
        {
            List<ConverSationResult> data = new List<ConverSationResult>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_list_conversation", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", userid));
                using (var dbreader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    while (dbreader.Read())
                    {
                        data.Add(new ConverSationResult()
                        {
                            Id = Convert.ToInt32(dbreader["Id"]),
                            ConverSationName = dbreader["ConverSationName"] != DBNull.Value ? Convert.ToString(dbreader["ConverSationName"]) : "",
                            LastMess = dbreader["Content"] != DBNull.Value ? Convert.ToString(dbreader["Content"]) : "",
                            CreatorId = dbreader["CreatorId"] != DBNull.Value ? Convert.ToInt32(dbreader["CreatorId"]) : 0,
                            TimeSpan = dbreader["CreateAt"] != DBNull.Value ? Time(Convert.ToDateTime(dbreader["CreateAt"])) : "",
                            Users = GetUsers(Convert.ToInt32(dbreader["Id"]), userid),
                            Seen = dbreader["Seen"] != DBNull.Value ? Convert.ToInt32(dbreader["Seen"]) : 1
                        }) ;
                    }
                    dbreader.Close();
                }
                connection.Close();
                
            }
            return data;
        }
        /// <summary>
        /// User đã tham gia cuộc trò chuyện
        /// </summary>
        /// <param name="conversationId"></param>
        /// <returns></returns>
        public List<UserEntity> GetUsers(int conversationId,int currentuserid)
        {
            List<UserEntity> data = new List<UserEntity>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_list_conversation_users", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@conversation_id", conversationId));
                cmd.Parameters.Add(CreateParameter("@userid", currentuserid));
                using (var dbreader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    while (dbreader.Read())
                    {
                        data.Add(new UserEntity()
                        {
                            UserId = Convert.ToInt32(dbreader["userId"]),
                            UserLastName = Convert.ToString(dbreader["userLastName"]),
                            Avartar = Convert.ToString(dbreader["avatar"])
                        });
                    }
                    dbreader.Close();
                }
                connection.Close();
            }
            
                return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowCount"></param>
        /// <param name="Name"></param>
        /// <returns></returns>
        public IList<Messages> ListMessage(MessageSearchingCondition model, out long rowCount,out string Name)
        {
            List<Messages> data = new List<Messages>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_list_message", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@conversationId", model.ConverSationId);
                cmd.Parameters.AddWithValue("@userid", model.userid);
                cmd.Parameters.AddWithValue("@Page", model.Page);
                cmd.Parameters.AddWithValue("@PageSize", model.PageSize);
                cmd.Parameters.Add(CreateOutputParameter("@rowcount", SqlDbType.BigInt));
                cmd.Parameters.Add(CreateOutputParameter("@converSationName", SqlDbType.NVarChar, 4000)).Direction = ParameterDirection.Output;
                using (var dbreader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    while (dbreader.Read())
                    {
                        data.Add(new Messages()
                        {
                            SenderId = Convert.ToInt32(dbreader["SenderId"]),
                            UserLastName = Convert.ToString(dbreader["userLastName"]),
                            Avartar = Convert.ToString(dbreader["avatar"]),
                            Content = Convert.ToString(dbreader["Content"]),
                            Id = Convert.ToInt32(dbreader["Id"]),
                            ConverSationID = Convert.ToInt32(dbreader["ConverSationID"]),
                            TimeSpan = Time(Convert.ToDateTime(dbreader["CreateAt"])),
                        });
                    }
                    dbreader.Close();
                }
                rowCount = Convert.ToInt64(cmd.Parameters["@rowCount"].Value);
                Name = Convert.ToString(cmd.Parameters["@converSationName"].Value);
                connection.Close();
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int Send(MessageInput data)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("[proc_create_new_message_people]", connection); 
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid1", data.SenderId));
                cmd.Parameters.Add(CreateParameter("@conversationidinput", data.ConverSationID));
                cmd.Parameters.Add(CreateParameter("@userid2", data.userid));
                cmd.Parameters.Add(CreateParameter("@content", data.Content));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="conversationid"></param>
        /// <returns></returns>
        public bool UpdateSeen(int userid,int conversationid)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("[proc_update_seen]", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@senderid", userid));
                cmd.Parameters.Add(CreateParameter("@conversationid", conversationid));
                result = cmd.ExecuteNonQuery()>0;
                connection.Close();
            }
            return result;
        }
    }
}
