﻿using Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class UserInGroupDAL:_BaseDAL,IUserInGroupDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        public UserInGroupDAL(string connectionString): base(connectionString) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="useringroup"></param>
        /// <returns></returns>
        public int Add(UserInGroup useringroup)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_add_user_in_group", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@groupid", useringroup.GroupID));
                cmd.Parameters.Add(CreateParameter("@userid", useringroup.UserId));
                cmd.Parameters.Add(CreateParameter("@roleid", useringroup.RoleID));
                cmd.Parameters.Add(CreateParameter("@status", useringroup.Status));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }

            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="groupid"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool Delete(int userid, int groupid)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_delete_user_in_group", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@groupid", groupid));
                cmd.Parameters.Add(CreateParameter("@userid", userid));
                result = cmd.ExecuteNonQuery() > 0;
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="useringroup"></param>
        /// <returns></returns>
        public bool Update(UserInGroup useringroup)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_update_user_in_group", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@id", useringroup.UserInGroupId));
                cmd.Parameters.Add(CreateParameter("@groupid", useringroup.GroupID));
                cmd.Parameters.Add(CreateParameter("@userid", useringroup.UserId));
                cmd.Parameters.Add(CreateParameter("@status", useringroup.Status));
                cmd.Parameters.Add(CreateParameter("@roleid", useringroup.RoleID));
                result = cmd.ExecuteNonQuery() > 0;
            }
            return result;
        }
        public bool UpdateSuperAdmin(int userid,int groupid)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_update_super_admin", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@groupid", groupid));
                cmd.Parameters.Add(CreateParameter("@userid", userid));
                result = cmd.ExecuteNonQuery() > 0;
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="groupid"></param>
        /// <returns></returns>
        public UserInGroup Get(int userid,int groupid)
        {
            UserInGroup data = new UserInGroup();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_get_user_in_group", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@groupid", groupid));
                cmd.Parameters.Add(CreateParameter("@userid", userid));
                using (var dbreader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    if(dbreader.Read())
                    {
                        data = new UserInGroup()
                        {
                            UserInGroupId = Convert.ToInt32(dbreader["UserInGroupId"]),
                            UserId = Convert.ToInt32(dbreader["UserID"]),
                            GroupID = Convert.ToInt32(dbreader["GroupID"]),
                            RoleID = Convert.ToInt32(dbreader["RoleID"]),
                            Status = Convert.ToInt32(dbreader["Status"])
                        };
                    }                  
                }                
            }
            return data;
        }

        public IList<UserInGroup> UserInGroupList(UserInGroupSearchingCondition model, out long rowCount)
        {
            List<UserInGroup> data = new List<UserInGroup>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("[proc_user_in_group_list]", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@Page", model.Page));
                cmd.Parameters.Add(CreateParameter("@PageSize", model.PageSize));
                cmd.Parameters.Add(CreateParameter("@status", model.Status));
                cmd.Parameters.Add(CreateParameter("@groupid", model.GroupID));
                cmd.Parameters.Add(CreateOutputParameter("@rowCount", SqlDbType.BigInt));
                using (var dbreader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbreader.Read())
                    {
                        data.Add(new UserInGroup()
                        {
                            UserInGroupId = Convert.ToInt32(dbreader["UserInGroupId"]),
                            GroupID = Convert.ToInt32(dbreader["GroupID"]),
                            UserId = Convert.ToInt32(dbreader["userId"]),
                            UserFirstName = Convert.ToString(dbreader["userFirstName"]),
                            UserLastName = Convert.ToString(dbreader["userLastName"]),
                            Avartar = Convert.ToString(dbreader["avatar"]),
                            RoleID = Convert.ToInt32(dbreader["RoleID"]),
                            Status = Convert.ToInt32(dbreader["Status"]),
                            SupperAdmin = Convert.ToInt32(dbreader["SuperAdmin"])
                        });
                    }
                    dbreader.Close();
                }
                rowCount = Convert.ToInt64(cmd.Parameters["@rowCount"].Value);

            }
            return data;
        }
    }
}
