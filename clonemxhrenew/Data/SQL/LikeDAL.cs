﻿using Core;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class LikeDAL:_BaseDAL,ILikeDAL
    {
        public LikeDAL(string Conectionstring) : base(Conectionstring) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int Add(Like data)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_like_post", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", data.UserId));
                cmd.Parameters.Add(CreateParameter("@postid", data.PostID));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int Count(int id)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("post_like_count", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@postid", id));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public Like GetLike(int id, int userid)
        {
            Like data  = new Like();
            using (SqlConnection connection = GetConnection()) { 
                SqlCommand cmd = CreateCommand("proc_get_like", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@postid", id));
                cmd.Parameters.Add(CreateParameter("@userid", userid));
               using(var dbreader  = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    if (dbreader.Read())
                    {
                        data = new Like()
                        {
                            LikeID = Convert.ToInt32(dbreader["LikeID"]),
                            UserId = Convert.ToInt32(dbreader["UserID"]),
                            PostID = Convert.ToInt32(dbreader["PostID"])
                        };
                    }
                }
               return data;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public bool UnLike(int id,int userid)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_un_like_post", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@id", id));
                cmd.Parameters.Add(CreateParameter("@userid", userid));
                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IList<UserEntity> UsersLikePost(int id)
        {
            List<UserEntity> data = new List<UserEntity>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("user_likepost_list", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@postid", id));
                using (var dbreader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    while (dbreader.Read())
                    {
                        data.Add(new UserEntity()
                        {
                            UserId = Convert.ToInt32(dbreader["userId"]),
                            UserFirstName = Convert.ToString(dbreader["userFirstName"]),
                            UserLastName = Convert.ToString(dbreader["userLastName"]),
                            Avartar = Convert.ToString(dbreader["avatar"])
                        });
                    }
                }
            }
            return data;
        }
    }
}
