﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace Data.SQL
{
    public class _BaseDAL
    {
        #region Constructor
        /// <summary>
        /// Chuỗi kết nối
        /// </summary>
        protected string connectionString = "";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connectionString"></param>
        public _BaseDAL(string connectionString)
        {
            this.connectionString = connectionString;
        }

        /// <summary>
        /// Khởi tạo và mở kết nối
        /// </summary>
        /// <returns></returns>
        protected SqlConnection GetConnection()
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            return conn;
        }
        #endregion
        /// <summary>
        /// ImageList
        /// </summary>
        /// <param name="postid"></param>
        /// <returns></returns>
        public List<Image> ImageList(int postid)
        {
            List<Image> data = new List<Image>();
            using (SqlConnection connection = GetConnection())
            {

                SqlCommand cmd = new SqlCommand("proc_image_of_post_list", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@postid", postid));
                SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dbReader.Read())
                {
                    data.Add(new Image()
                    {
                        PostID = Convert.ToInt32(dbReader["PostID"]),
                        ImageID = Convert.ToInt32(dbReader["ImageID"]),
                        Url = Convert.ToString(dbReader["Url"])
                    });
                }
                connection.Close();
            }
            return data;
        }
        #region SqlCommand

        /// <summary>
        /// Tạo đối tượng SqlCommand sử dụng Stored Procedure làm Command Text
        /// </summary>
        /// <param name="procedureName">Tên của stored procedure</param>
        /// <param name="connection">Đối tượng SqlConnection</param>
        /// <returns></returns>
        protected SqlCommand CreateCommand(string procedureName, SqlConnection connection)
        {
            SqlCommand cmd = new SqlCommand(procedureName, connection);
            cmd.CommandType = CommandType.StoredProcedure;
            return cmd;
        }
        /// <summary>
        /// Tạo đối tượng SqlCommand
        /// </summary>
        /// <param name="commandText">Chuỗi câu lệnh</param>
        /// <param name="connection">Đối tượng SqlConnection</param>
        /// <param name="commandType">Kiểu của câu lệnh</param>
        /// <returns></returns>
        protected SqlCommand CreateCommand(string commandText, SqlConnection connection, CommandType commandType)
        {
            SqlCommand cmd = new SqlCommand(commandText, connection);
            cmd.CommandType = commandType;
            return cmd;
        }
        /// <summary>
        /// Chuyển thời gian về giờ phút giây
        /// </summary>
        /// <param name="createAt"></param>
        /// <returns></returns>
        protected string Time(DateTime createAt)
        {
            DateTime currentTime = DateTime.Now;
            TimeSpan elapsedTime = currentTime - createAt;
            int daysDifference =(int)elapsedTime.TotalDays;

            // Tính số giờ, phút, giây từ timeDifference
            int hoursDifference = (int)elapsedTime.TotalHours;
            int minutesDifference = (int)elapsedTime.TotalMinutes;
            int secondsDifference = (int)elapsedTime.TotalSeconds;

            if (daysDifference>=1)
            {
                return $"{daysDifference} ngày trước";
            }
            else if (hoursDifference >=1)
            {
                return $"{hoursDifference} giờ trước";
            }
            else if(minutesDifference >= 1)
            {
                return $"{minutesDifference} phút trước";
            }
            else
            {
                return $"{secondsDifference} giây trước";
            }
        }

        #endregion

        #region Parameters

        /// <summary>
        /// Tạo 1 para gồm tên và giá trị truyền vào
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected SqlParameter CreateParameter(string parameterName, object value)
        {
            if (value == null)
                value = DBNull.Value;
            SqlParameter para = new SqlParameter(parameterName, value);
            return para;
        }

        /// <summary>
        /// Tạo tham số
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="dbType"></param>
        /// <returns></returns>
        protected SqlParameter CreateParameter(string parameterName, SqlDbType dbType)
        {
            SqlParameter para = new SqlParameter(parameterName, dbType);
            return para;
        }
        /// <summary>
        /// Tạo tham số
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="dbType"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected SqlParameter CreateParameter(string parameterName, SqlDbType dbType, object value)
        {
            if (value == null)
                value = DBNull.Value;
            if (dbType == SqlDbType.DateTime && Convert.ToDateTime(value).Equals(DateTime.MinValue))
                value = DBNull.Value;
            SqlParameter para = new SqlParameter(parameterName, dbType);
            para.Value = value;
            return para;
        }
        /// <summary>
        /// Tạo tham số với giá trị size
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="dbType"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        protected SqlParameter CreateParameter(string parameterName, SqlDbType dbType, int size, object value)
        {
            if (value == null)
                value = DBNull.Value;
            SqlParameter para = new SqlParameter(parameterName, dbType, size);
            para.Value = value;
            return para;
        }
        /// <summary>
        /// Tạo tham số kiểu return, chỉ cho phép kiểu INT32
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="dbType"></param>
        /// <returns></returns>
        protected SqlParameter CreateReturnValueParameter(string parameterName)
        {
            SqlParameter para = new SqlParameter(parameterName, SqlDbType.Int);
            para.Direction = ParameterDirection.ReturnValue;
            return para;
        }

        /// <summary>
        /// Tạo tham số kiểu output
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="dbType"></param>
        /// <returns></returns>
        protected SqlParameter CreateOutputParameter(string parameterName, SqlDbType dbType)
        {
            SqlParameter para = new SqlParameter();
            para.ParameterName = parameterName;
            para.SqlDbType = dbType;
            para.Direction = ParameterDirection.Output;
            return para;
        }
        /// <summary>
        /// Tạo tham số kiểu output với size truyền vào
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="paraType"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        protected SqlParameter CreateOutputParameter(string parameterName, SqlDbType dbType, int size)
        {
            SqlParameter para = new SqlParameter();
            para.ParameterName = parameterName;
            para.SqlDbType = dbType;
            para.Size = size;
            para.Direction = ParameterDirection.Output;
            return para;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="dbType"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        protected SqlParameter CreateOutputParameter(string parameterName, SqlDbType dbType, int size, object value)
        {
            if (value == null)
                value = DBNull.Value;
            SqlParameter para = new SqlParameter();
            para.ParameterName = parameterName;
            para.SqlDbType = dbType;
            para.Size = size;
            para.Value = value;
            para.Direction = ParameterDirection.Output;
            return para;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        protected SqlParameter CreateValueTableParameter(string parameterName, IEnumerable<string> values)
        {
            DataTable dt = new DataTable("ValueTable");
            dt.Columns.Add(new DataColumn("Value", typeof(string)));
            if (values != null)
            {
                foreach (var value in values)
                {
                    dt.Rows.Add(value);
                }
                dt.AcceptChanges();
            }
            SqlParameter para = new SqlParameter();
            para.ParameterName = parameterName;
            para.SqlDbType = SqlDbType.Structured;
            para.Value = dt;

            return para;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        protected SqlParameter CreateKeyValueTableParameter(string parameterName, Dictionary<string, string> values)
        {
            DataTable dt = new DataTable("ValueTable");
            dt.Columns.Add(new DataColumn("Key", typeof(string)));
            dt.Columns.Add(new DataColumn("Value", typeof(string)));
            if (values != null)
            {
                foreach (var value in values)
                {
                    dt.Rows.Add(value.Key, value.Value);
                }
                dt.AcceptChanges();
            }
            SqlParameter para = new SqlParameter();
            para.ParameterName = parameterName;
            para.SqlDbType = SqlDbType.Structured;
            para.Value = dt;

            return para;
        }

        #endregion

        #region Utils

        /// <summary>
        /// Kiểm tra 1 giá trị datetime dc lấy từ db, nếu null trả về 1/1/1900
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected DateTime SetDateTime(object value)
        {
            return value == DBNull.Value ? new DateTime(1900, 1, 1) : Convert.ToDateTime(value);
        }

        #endregion
    }
}
