﻿using Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class SearchDAL : _BaseDAL, ISearchDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConnectiongString"></param>
        public SearchDAL(string ConnectiongString) : base(ConnectiongString) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IList<Search> Result(SearchCondition model,out long rowCount)
        {
            List<Search> results = new List<Search>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("proc_search", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@searchText", model.SearchText);
                cmd.Parameters.AddWithValue("@groupid", model.GroupID);
                cmd.Parameters.AddWithValue("@userid", model.UserId);
                cmd.Parameters.Add(CreateOutputParameter("@rowCount", SqlDbType.BigInt));
                using (var dbreader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbreader.Read())
                    {
                        results.Add(new Search()
                        {
                            Type = dbreader["Type"] != DBNull.Value ? Convert.ToString(dbreader["Type"]) : "",
                            Avatar = dbreader["Avatar"] != DBNull.Value ? Convert.ToString(dbreader["avatar"]) : "",
                            Content = dbreader["Content"] != DBNull.Value ? Convert.ToString(dbreader["Content"]) : "",
                            GroupName = dbreader["GroupName"] != DBNull.Value ? Convert.ToString(dbreader["GroupName"]) : "",
                            GroupPicture = dbreader["GroupPicture"] != DBNull.Value ? Convert.ToString(dbreader["GroupPicture"]) : "",
                            UserFirstName = dbreader["userFirstName"] != DBNull.Value ? Convert.ToString(dbreader["userFirstName"]) : "",
                            UserLastName = dbreader["userLastName"] != DBNull.Value ? Convert.ToString(dbreader["userLastName"]) : "",
                            GroupID = dbreader["GroupID"] != DBNull.Value ? Convert.ToInt32(dbreader["GroupID"]) : 0,
                            PostID = dbreader["PostID"] != DBNull.Value ? Convert.ToInt32(dbreader["PostID"]) : 0,
                            RoleID = dbreader["PostID"] != DBNull.Value ? Convert.ToInt32(dbreader["PostID"]) : 0,
                            SuperAdmin = dbreader["SuperAdmin"] != DBNull.Value ? Convert.ToInt32(dbreader["SuperAdmin"]) : 0,
                            UserID = dbreader["userid"] != DBNull.Value ? Convert.ToInt32(dbreader["userid"]) : 0,
                            PostPrivacyMode = dbreader["PostPrivacyMode"] != DBNull.Value ? Convert.ToInt32(dbreader["PostPrivacyMode"]) : 0,                           
                            TimeSpan = dbreader["CreateAt"] != DBNull.Value ? Time(Convert.ToDateTime(dbreader["CreateAt"])) : "",
                            LikeCount = dbreader["LikesCount"] != DBNull.Value ? Convert.ToInt32(dbreader["LikesCount"]) : 0,
                            CommentCount = dbreader["CommentsCount"] != DBNull.Value ? Convert.ToInt32(dbreader["CommentsCount"]) : 0
                        });
                    }
                    dbreader.Close();
                }
                connection.Close();
                rowCount = Convert.ToInt64(cmd.Parameters["@rowCount"].Value != DBNull.Value ? cmd.Parameters["@rowCount"].Value : 0);
            }
            return results;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public IList<PostEx> ListPost(PostSearchingCondition model, out long rowcount)
        {
            List<PostEx> data = new List<PostEx>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand("proc_search_post", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", model.UserId);
                cmd.Parameters.AddWithValue("@page", model.Page);
                cmd.Parameters.AddWithValue("@pagesize", model.PageSize);
                cmd.Parameters.AddWithValue("@groupid", model.GroupID);
                cmd.Parameters.AddWithValue("@searchText", model.SearchText);
                cmd.Parameters.Add(CreateOutputParameter("@rowcount", SqlDbType.BigInt));
                using (var dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new PostEx()
                        {
                            PostID = Convert.ToInt32(dbReader["PostID"]),
                            Content = Convert.ToString(dbReader["Content"]),
                            UserId = Convert.ToInt32(dbReader["userId"]),
                            UserFirstName = Convert.ToString(dbReader["userFirstName"]),
                            UserLastName = Convert.ToString(dbReader["userLastName"]),
                            Avartar = Convert.ToString(dbReader["avatar"]),
                            GroupID = dbReader["GroupID"]!=DBNull.Value? Convert.ToInt32(dbReader["GroupID"]):0,
                            CreateAt = Convert.ToDateTime(dbReader["CreateAt"]),
                            Images = ImageList(Convert.ToInt32(dbReader["PostID"])),
                            TimeSpan = Time(Convert.ToDateTime(dbReader["CreateAt"])),
                            SuperAdmin = dbReader["SuperAdmin"] != DBNull.Value ? Convert.ToInt32(dbReader["SuperAdmin"]) : 0,
                            GroupName = dbReader["GroupName"] != DBNull.Value ? Convert.ToString(dbReader["GroupName"]) : "",
                            RoleID = dbReader["RoleID"] != DBNull.Value ? Convert.ToInt32(dbReader["RoleID"]) : 0,
                            GroupPicture = dbReader["GroupPicture"] != DBNull.Value ? Convert.ToString(dbReader["GroupPicture"]) : "",
                            UsersLikePost = dbReader["LikesCount"] != DBNull.Value ? Convert.ToInt32(dbReader["LikesCount"]) : 0,
                            CommentCount = dbReader["CommentsCount"] != DBNull.Value ? Convert.ToInt32(dbReader["CommentsCount"]) : 0
                        });
                    }
                    dbReader.Close();
                }
                connection.Close();
                rowcount = Convert.ToInt64(cmd.Parameters["@rowcount"].Value != DBNull.Value ? cmd.Parameters["@rowcount"].Value : 0);
            }
            return data;
        }

    }

}
