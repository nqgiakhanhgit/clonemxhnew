﻿using Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class FollowDAL : _BaseDAL, IFollowDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        public FollowDAL(string connectionString) : base(connectionString) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fl"></param>
        /// <returns></returns>
        public int AddFollow(Follow fl)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_add_follow", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid1", fl.UserId1));
                cmd.Parameters.Add(CreateParameter("@userid2", fl.UserId2)); ;
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        public Follow CheckFollow(int userid1, int userid2)
        {
            Follow data = new Follow();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_check_follow", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid1", userid1));
                cmd.Parameters.Add(CreateParameter("@userid2", userid2));
                using (var dbreader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbreader.Read())
                    {
                        data = new Follow()
                        {
                            FollowId = Convert.ToInt32(dbreader["FollowID"]),
                            UserId1 = Convert.ToInt32(dbreader["UserID1"]),
                            UserId2 = Convert.ToInt32(dbreader["UserID2"])
                        };
                    }
                }
                connection.Close();
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int FollowCount(int id)
        {
                int count = 0;
                using (SqlConnection connection = GetConnection())
                {
                    SqlCommand cmd = CreateCommand("proc_follow_count", connection);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(CreateParameter("@userid", id));
                    count = Convert.ToInt32(cmd.ExecuteScalar());
                    connection.Close();
                }
                return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int FollowerCount(int id)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_follower_count", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", id));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public IList<UserEntity> FollowerList(UserSearchingCondition mode, out long rowcount)
        {
            List<UserEntity> data = new List<UserEntity>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("proc_list_follower", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", mode.UserId));
                cmd.Parameters.Add(CreateParameter("@page", mode.Page));
                cmd.Parameters.Add(CreateParameter("@pagesize", mode.PageSize));
                cmd.Parameters.Add(CreateOutputParameter("@rowcount", SqlDbType.BigInt));
                SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dbReader.Read())
                {
                    data.Add(new UserEntity()
                    {
                        UserId = Convert.ToInt32(dbReader["userId"]),
                        UserFirstName = Convert.ToString(dbReader["userFirstName"]),
                        UserLastName = Convert.ToString(dbReader["userLastName"]),
                        Avartar = Convert.ToString(dbReader["avatar"])
                    });
                }
                connection.Close();
                rowcount = Convert.ToInt64(cmd.Parameters["@RowCount"].Value);
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public IList<UserEntity> FollowList(UserSearchingCondition model, out long rowcount)
        {

            List<UserEntity> data = new List<UserEntity>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("proc_list_follow", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", model.UserId));
                cmd.Parameters.Add(CreateParameter("@page", model.Page));
                cmd.Parameters.Add(CreateParameter("@pagesize", model.PageSize));
                cmd.Parameters.Add(CreateOutputParameter("@rowcount", SqlDbType.BigInt));
                SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dbReader.Read())
                {
                    data.Add(new UserEntity()
                    {
                        UserId  = Convert.ToInt32(dbReader["userId"]),
                        UserFirstName = Convert.ToString(dbReader["userFirstName"]),
                        UserLastName = Convert.ToString(dbReader["userLastName"]),
                        Avartar = Convert.ToString(dbReader["avatar"])

                    });
                }
                connection.Close();
                rowcount = Convert.ToInt64(cmd.Parameters["@RowCount"].Value);
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        public bool UnFollow(int userid1, int userid2)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_un_follow", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid1", userid1));
                cmd.Parameters.Add(CreateParameter("@userid2", userid2));
                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }
            return result;
        }
    }
}
