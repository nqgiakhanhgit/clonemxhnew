﻿using Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class FriendDAL : _BaseDAL, IFriendDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        public FriendDAL(string connectionString) : base(connectionString) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="friend"></param>
        /// <returns></returns>
        public int AddFriend(Friend friend)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_add_friend", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid1", friend.UserID1));
                cmd.Parameters.Add(CreateParameter("@userid2", friend.UserID2));
                cmd.Parameters.Add(CreateParameter("@status", friend.Status));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        public bool UnFriend(int userid1, int userid2)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_un_friend", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid1", userid1));
                cmd.Parameters.Add(CreateParameter("@userid2", userid2));
                result = cmd.ExecuteNonQuery()>0;
                connection.Close();
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public List<UserEntity> FriendList(UserSearchingCondition mode,out long rowcount)
        {
            List<UserEntity> data = new List<UserEntity>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("proc_friend_list", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", mode.UserId));
                cmd.Parameters.Add(CreateParameter("@page", mode.Page));
                cmd.Parameters.Add(CreateParameter("@pagesize", mode.PageSize));
                cmd.Parameters.Add(CreateParameter("@Status", mode.Status));
                cmd.Parameters.Add(CreateOutputParameter("@rowcount", SqlDbType.BigInt));
                SqlDataReader dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dbReader.Read())
                {
                    data.Add(new UserEntity()
                    {
                        UserId = Convert.ToInt32(dbReader["userId"]),
                        UserFirstName = Convert.ToString(dbReader["userFirstName"]),
                        UserLastName = Convert.ToString(dbReader["userLastName"]),
                        Avartar = Convert.ToString(dbReader["avatar"])
                    });
                }
                connection.Close();
                rowcount = Convert.ToInt64(cmd.Parameters["@rowcount"].Value);
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        public Friend GetFriend(int userid1, int userid2)
        {
            Friend friend = new Friend();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_get_friend", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid1", userid1));
                cmd.Parameters.Add(CreateParameter("@userid2", userid2));
                using (SqlDataReader reader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                {
                    if (reader.Read())
                    {
                        friend = new Friend()
                        {
                                FriendID = Convert.ToInt32(reader["FriendID"]),
                                UserID1 = Convert.ToInt32(reader["UserID1"]),
                                UserID2 = Convert.ToInt32(reader["UserID2"]),
                                Status = Convert.ToInt32(reader["Status"])
                        };
                    }
                }
                connection.Close();
            }
            return friend;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid1"></param>
        /// <param name="userid2"></param>
        /// <returns></returns>
        public bool Acept(int userid1 ,int userid2)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_accept_friend", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid1", userid1));
                cmd.Parameters.Add(CreateParameter("@userid2", userid2));
                cmd.Parameters.Add(CreateParameter("@status",StatusFriend.Acept));
                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int Count(int id)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_friend_count", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", id));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
    }
}
