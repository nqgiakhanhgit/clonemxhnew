﻿using Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class UserDAL : _BaseDAL, IUserDAL
    {
        public UserDAL(string connectionString): base(connectionString) { }
        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public UserEntity Login(string userEmail, string password)
        {
            UserEntity data = new UserEntity();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_user_login", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@email", userEmail);
                cmd.Parameters.AddWithValue("@pass", password);
                using (var dbreader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if(dbreader.Read())
                    {
                        data = new UserEntity()
                        {
                            UserId = Convert.ToInt32(dbreader["userId"]),
                            UserFirstName = Convert.ToString(dbreader["userFirstName"]),
                            UserLastName = Convert.ToString(dbreader["userLastName"]),
                            BirthDay = Convert.ToDateTime(dbreader["birthday"]),
                            WallPaper = Convert.ToString(dbreader["wallPaper"]),
                            Avartar = Convert.ToString(dbreader["avatar"]),
                            Sex = Convert.ToInt32(dbreader["sex"]),
                            Email = Convert.ToString(dbreader["userEmail"]),
                            Password = Convert.ToString(dbreader["userPassword"]),
                            CreateDay = Convert.ToDateTime(dbreader["createday"]),
                            Phone = Convert.ToString(dbreader["userPhone"])
                
                        };
                    }
                    dbreader.Close();
                }
                connection.Close();

            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Register(UserEntity user)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_user_register", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@UserFirstName", user.UserFirstName));
                cmd.Parameters.Add(CreateParameter("@UserLastName", user.UserLastName));
                cmd.Parameters.Add(CreateParameter("@BirthDay", user.BirthDay));
                cmd.Parameters.Add(CreateParameter("@Password", user.Password));
                cmd.Parameters.Add(CreateParameter("@Email", user.Email));
                cmd.Parameters.Add(CreateParameter("@Phone", user.Phone));
                cmd.Parameters.Add(CreateParameter("@WallPaper", user.WallPaper));
                cmd.Parameters.Add(CreateParameter("@Avartar", user.Avartar));
                cmd.Parameters.Add(CreateParameter("@Sex", user.Sex));
                cmd.Parameters.Add(CreateParameter("@CreateDay", user.CreateDay));
                count = Convert.ToInt32 (cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool Update(UserEntity user)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_update_profile", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@firstname", user.UserFirstName));
                cmd.Parameters.Add(CreateParameter("@lastname", user.UserLastName));
                cmd.Parameters.Add(CreateParameter("@birthday", user.BirthDay));
                cmd.Parameters.Add(CreateParameter("@phone", user.Phone));
                cmd.Parameters.Add(CreateParameter("@sex", user.Sex));
                cmd.Parameters.Add(CreateParameter("@location  ", user.Location));
                cmd.Parameters.Add(CreateParameter("@education", user.Education));
                cmd.Parameters.Add(CreateParameter("@skill", user.Skill));
                cmd.Parameters.Add(CreateParameter("@note", user.Note));
                cmd.Parameters.Add(CreateParameter("@id", user.UserId));
                result = cmd.ExecuteNonQuery()>0;
                connection.Close();
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <returns></returns>
        public bool CheckUserExits(string email, string phone)
        {
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_check_user_exist", connection);
                cmd.Connection = connection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@phone", phone);
                object retustl = cmd.ExecuteScalar();
                connection.Close();
                if (retustl != null)
                {
                    return true;
                }
                else return false;

            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserEx Get(int id)
        {
            UserEx data = new UserEx();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_get_profile", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                using (var dbreader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbreader.Read())
                    {
                        data = new UserEx()
                        {
                            UserId = Convert.ToInt32(dbreader["userId"]),
                            UserFirstName = Convert.ToString(dbreader["userFirstName"]),
                            UserLastName = Convert.ToString(dbreader["userLastName"]),
                            BirthDay = Convert.ToDateTime(dbreader["birthday"]),
                            WallPaper = Convert.ToString(dbreader["wallPaper"]),
                            Avartar = Convert.ToString(dbreader["avatar"]),
                            Sex = Convert.ToInt32(dbreader["sex"]),
                            Email = Convert.ToString(dbreader["userEmail"]),
                            Password = Convert.ToString(dbreader["userPassword"]),
                            CreateDay = Convert.ToDateTime(dbreader["createday"]),
                            Phone = Convert.ToString(dbreader["userPhone"]),
                            Location =dbreader["Location"]!=DBNull.Value? Convert.ToString(dbreader["Location"]):"",
                            Note = dbreader["Note"] != DBNull.Value ? Convert.ToString(dbreader["Note"]):"",
                            Education = dbreader["Education"]!=DBNull.Value ? Convert.ToString(dbreader["Education"]):"",
                            Skill = dbreader["Skill"] != DBNull.Value ? Convert.ToString(dbreader["Skill"]):""

                        };
                    }
                    dbreader.Close();
                }
                connection.Close();

            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public UserEntity GetUserByEmail(string email)
        {
            UserEntity data = new UserEntity();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_get_user_by_email", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@email", email);
                using (var dbreader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbreader.Read())
                    {
                        data = new UserEntity()
                        {
                            UserId = Convert.ToInt32(dbreader["userId"]),
                            UserFirstName = Convert.ToString(dbreader["userFirstName"]),
                            UserLastName = Convert.ToString(dbreader["userLastName"]),
                            BirthDay = Convert.ToDateTime(dbreader["birthday"]),
                            WallPaper = Convert.ToString(dbreader["wallPaper"]),
                            Avartar = Convert.ToString(dbreader["avatar"]),
                            Sex = Convert.ToInt32(dbreader["sex"]),
                            Email = Convert.ToString(dbreader["userEmail"]),
                            CreateDay = Convert.ToDateTime(dbreader["createday"]),
                            Phone = Convert.ToString(dbreader["userPhone"])

                        };
                    }
                    dbreader.Close();
                }
                connection.Close();

            }
            return data;
        }

        public string GetImage(int id, int imagetype)
        {
            string data = null;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                if (imagetype == ImageType.Wall)
                {
                     cmd = CreateCommand("proc_get_wallpaper", connection);
                }
                else
                {
                    if(imagetype == ImageType.Avatar)
                    {
                         cmd = CreateCommand("proc_get_avatar", connection);
                    }
                }
               
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", id);
                using (var dbreader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbreader.Read())
                    {
                        data = Convert.ToString(dbreader["ImageProfile"]);                       
                    }
                    dbreader.Close();
                }
                connection.Close();

            }
            return data;
        }

        public bool UpdateImage(int id, string image, int imagetype)
        {
            bool data = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                if (imagetype == ImageType.Wall)
                {
                    cmd = CreateCommand("proc_update_wallpaper", connection);
                }
                else
                {
                    if (imagetype == ImageType.Avatar)
                    {
                        cmd = CreateCommand("proc_update_avatar", connection);
                    }
                }

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", id);
                cmd.Parameters.AddWithValue("@image", image);
                data = cmd.ExecuteNonQuery()>0;
                connection.Close();

            }
            return data;
        }
    }
}
