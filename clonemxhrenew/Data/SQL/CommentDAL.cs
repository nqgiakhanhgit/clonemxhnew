﻿using Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class CommentDAL : _BaseDAL, ICommentDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        public CommentDAL(string connectionString) : base(connectionString) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmt"></param>
        /// <returns></returns>
        public int Add(Comment cmt)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_add_comment", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", cmt.UserId));
                cmd.Parameters.Add(CreateParameter("@content", cmt.Content));
                cmd.Parameters.Add(CreateParameter("@parentid", cmt.ParentID));
                cmd.Parameters.Add(CreateParameter("@postid", cmt.PostID));
                cmd.Parameters.Add(CreateParameter("@createat", cmt.CreateAt));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int Count(int id)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("post_comment_count", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@postid", id));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool Delete(int id)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_delete_comment", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@commentid", id));
                result = cmd.ExecuteNonQuery()>0;
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Comment Get(int id)
        {
            Comment data = new Comment();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("proc_get_comment", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@commentid", id);
                using (var dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (dbReader.Read())
                    {
                        data = new Comment()
                        {
                            CommentID = Convert.ToInt32(dbReader["CommentID"]),
                            PostID = Convert.ToInt32(dbReader["PostID"]),
                            Content = Convert.ToString(dbReader["Content"]),
                            UserId = Convert.ToInt32(dbReader["userId"]),
                            UserFirstName = Convert.ToString(dbReader["userFirstName"]),
                            UserLastName = Convert.ToString(dbReader["userLastName"]),
                            Avartar = Convert.ToString(dbReader["avatar"]),
                            TimeSpan = Time(Convert.ToDateTime(dbReader["CreateAt"])),
                            RepCommentCount = RepCommentCount(Convert.ToInt32(dbReader["CommentID"])),
                            ParentID = dbReader["ParentID"]!=DBNull.Value? Convert.ToInt32(dbReader["ParentID"]):0
                        };
                    }
                    dbReader.Close();
                }
                connection.Close();
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        public IList<Comment> ListComment(CommentSearchingCondition model, out long rowCount,out int RoleID)
        {
            List<Comment> data = new List<Comment>();

            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("proc_comment_list", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", model.UserId);
                cmd.Parameters.AddWithValue("@postid", model.PostId);
                cmd.Parameters.AddWithValue("@Page", model.Page);
                cmd.Parameters.AddWithValue("@PageSize", model.PageSize);
                cmd.Parameters.AddWithValue("@parentid", model.ParentID);
                cmd.Parameters.Add(CreateOutputParameter("@roleid", 0));
                cmd.Parameters.Add(CreateOutputParameter("@rowCount", SqlDbType.BigInt));
                using (var dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new Comment()
                        {
                            CommentID = Convert.ToInt32(dbReader["CommentID"]),
                            PostID = Convert.ToInt32(dbReader["PostID"]),
                            Content = Convert.ToString(dbReader["Content"]),
                            UserId = Convert.ToInt32(dbReader["userId"]),
                            UserFirstName = Convert.ToString(dbReader["userFirstName"]),
                            UserLastName = Convert.ToString(dbReader["userLastName"]),
                            Avartar = Convert.ToString(dbReader["avatar"]),
                            TimeSpan = Time(Convert.ToDateTime(dbReader["CreateAt"])),
                            RepCommentCount =RepCommentCount(Convert.ToInt32(dbReader["CommentID"])),
                            ParentID = dbReader["ParentID"] != DBNull.Value ? Convert.ToInt32(dbReader["ParentID"]) : 0,

                        }) ;
                    }
                    dbReader.Close();
                }
                connection.Close();
                RoleID = cmd.Parameters["@roleid"].Value != DBNull.Value ? Convert.ToInt32(cmd.Parameters["@roleid"].Value) : 0;
                rowCount = cmd.Parameters["@rowCount"].Value != DBNull.Value ? Convert.ToInt64(cmd.Parameters["@rowCount"].Value) : 0;

            }
            return data;
        }
        public int RepCommentCount (int commentid)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_repcomment_count", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@commentid", commentid));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        public bool Update(Comment comment)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_update_comment", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@commentid", comment.CommentID));
                cmd.Parameters.Add(CreateParameter("@content", comment.Content));
                result = cmd.ExecuteNonQuery() > 0;
            }
            return result;
        }
    }
}
