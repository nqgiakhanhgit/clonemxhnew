﻿using Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Data.SQL
{
    public class PostDAL : _BaseDAL, IPostDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectingString"></param>
        public PostDAL(string connectingString) : base(connectingString) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public int Add(Post post)
        {
            int count = 0;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_add_post", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@userid", post.UserId));
                cmd.Parameters.Add(CreateParameter("@groupid", post.GroupID));
                cmd.Parameters.Add(CreateParameter("@content", post.Content));
                cmd.Parameters.Add(CreateParameter("@privacymode", post.PrivacyMode));
                cmd.Parameters.Add(CreateParameter("@createat", post.CreateAt));
                count = Convert.ToInt32(cmd.ExecuteScalar());
                connection.Close();
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_delete_post", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@id", id));
                result = cmd.ExecuteNonQuery()>0;
                connection.Close();
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PostEx Get(int id)
        {
            PostEx data = new PostEx();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand("proc_get_post", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                using (var dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data = new PostEx()
                        {
                            PostID = Convert.ToInt32(dbReader["PostID"]),
                            Content = Convert.ToString(dbReader["Content"]),
                            UserId = Convert.ToInt32(dbReader["userId"]),
                            UserFirstName = Convert.ToString(dbReader["userFirstName"]),
                            UserLastName = Convert.ToString(dbReader["userLastName"]),
                            Avartar = Convert.ToString(dbReader["avatar"]),
                            GroupID = Convert.ToInt32(dbReader["GroupID"]),
                            TimeSpan = Time(Convert.ToDateTime(dbReader["CreateAt"])),
                            PrivacyMode = Convert.ToInt32(dbReader["PrivacyMode"]),
                            UsersLikePost = dbReader["LikesCount"] != DBNull.Value ? Convert.ToInt32(dbReader["LikesCount"]) : 0,
                            CommentCount = dbReader["CommentsCount"] != DBNull.Value ? Convert.ToInt32(dbReader["CommentsCount"]) : 0,

                            Images = ImageList(id)
                        };
                    }
                    dbReader.Close();
                }
                connection.Close();
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="postType"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public IList<PostEx> ListPost(PostSearchingCondition model, out long rowcount, int postType)
        {
            List<PostEx> data = new List<PostEx>();
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = new SqlCommand();
                switch (postType)
                {
                    case 1:
                        cmd = new SqlCommand("proc_profile_post_list", connection);
                        break;
                    case 2:
                        cmd = new SqlCommand("proc_group_inside_post_list", connection);
                        break;
                    case 3:
                        cmd = new SqlCommand("proc_post_group_index_list", connection);
                        break;
                    case 4:
                        cmd = new SqlCommand("proc_post_home_list", connection);
                        break;
                    default: throw new NotImplementedException();
                }

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", model.UserId);
                cmd.Parameters.AddWithValue("@page", model.Page);
                cmd.Parameters.AddWithValue("@pagesize", model.PageSize);
                cmd.Parameters.AddWithValue("@privacymode", model.PrivacyMode);
                cmd.Parameters.AddWithValue("@groupid", model.GroupID);
                cmd.Parameters.Add(CreateOutputParameter("@rowcount", SqlDbType.BigInt));
                using (var dbReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (dbReader.Read())
                    {
                        data.Add(new PostEx()
                        {
                            PostID = Convert.ToInt32(dbReader["PostID"]),
                            Content = Convert.ToString(dbReader["Content"]),
                            UserId = Convert.ToInt32(dbReader["userId"]),
                            UserFirstName = Convert.ToString(dbReader["userFirstName"]),
                            UserLastName = Convert.ToString(dbReader["userLastName"]),
                            Avartar = Convert.ToString(dbReader["avatar"]),
                            GroupID = Convert.ToInt32(dbReader["GroupID"]),
                            CreateAt = Convert.ToDateTime(dbReader["CreateAt"]),
                            Images = ImageList(Convert.ToInt32(dbReader["PostID"])),
                            TimeSpan = Time(Convert.ToDateTime(dbReader["CreateAt"])),
                            SuperAdmin = dbReader["SuperAdmin"] != DBNull.Value ? Convert.ToInt32(dbReader["SuperAdmin"]) : 0,
                            GroupName = dbReader["GroupName"] != DBNull.Value ? Convert.ToString(dbReader["GroupName"]) : "",
                            RoleID = dbReader["RoleID"] != DBNull.Value ? Convert.ToInt32(dbReader["RoleID"]) : 0,
                            GroupPicture = dbReader["GroupPicture"] != DBNull.Value ? Convert.ToString(dbReader["GroupPicture"]) : "",
                            UsersLikePost = dbReader["LikesCount"] != DBNull.Value ? Convert.ToInt32(dbReader["LikesCount"]) : 0,
                            CommentCount = dbReader["CommentsCount"] != DBNull.Value ? Convert.ToInt32(dbReader["CommentsCount"]) : 0
                        });
                    }
                    dbReader.Close();
                }
                connection.Close();
                rowcount = Convert.ToInt64(cmd.Parameters["@rowcount"].Value);
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        public bool Update(Post post)
        {
            bool result = false;
            using (SqlConnection connection = GetConnection())
            {
                SqlCommand cmd = CreateCommand("proc_update_post", connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(CreateParameter("@content", post.Content));
                cmd.Parameters.AddWithValue("@postid", post.PostID);
                cmd.Parameters.AddWithValue("@privacymode", post.PrivacyMode);
                result = cmd.ExecuteNonQuery() > 0;
                connection.Close();
            }
            return result;
        }
    }
}
