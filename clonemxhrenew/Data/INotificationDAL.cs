﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface INotificationDAL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Add(NotificationInsert model);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rowCount"></param>
        /// <returns></returns>
        IList<NotificationResult> ListNotificationResult(NotificationSearchingCondition model, out long rowCount);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Count(int id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationid"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        bool Update(int notificationid, int status);
        /// <summary>
        /// /
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        NotificationResult Get(int id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notifier_id"></param>
        /// <returns></returns>
        bool Update_All_Noti(int notifier_id);
    }
}
